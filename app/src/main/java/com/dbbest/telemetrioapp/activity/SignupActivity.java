package com.dbbest.telemetrioapp.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.model.User;
import com.dbbest.telemetrioapp.model.UserSignup;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SignupActivity extends Fragment {

    private static final String TAG = SignupActivity.class.getSimpleName();

//    private LoginManager mLoginManager;

    private Context mContext;
    private FragmentActivityBackStack stack;
    private boolean isActive;
    private ProgressBar mProgressBar;
    private SharedPreference mSharedPreference;

    private String requestId;
    private BroadcastReceiver requestReceiver;


    public static SignupActivity newInstance() {
        SignupActivity fragment = new SignupActivity();
        fragment.stack = null;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity();

        mSharedPreference = new SharedPreference(mContext);
        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.signup_screen, container, false);

        final CheckBox tos_chk = (CheckBox) view.findViewById(R.id.chk);

        Button btn_create = (Button) view.findViewById(R.id.btn_create);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_indicator);

        final EditText input_mail = (EditText) view.findViewById(R.id.input_mail);
        final EditText input_password = (EditText) view.findViewById(R.id.input_password);


//        mLoginManager = LoginManager.getInstance(mContext);

        btn_create.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                if (TelemetryApplication.connectionIsAvaible()) {
                if (tos_chk.isChecked()) {
                    String iMail = input_mail.getText().toString();
                    final String iPassword = input_password.getText().toString();


                        if ((null != iMail && (!iMail.equals(""))) && (null != iPassword && (!iPassword.equals("")))) {
                            mProgressBar.setVisibility(View.VISIBLE);

                            String regEMail = "@mail.com";
                            if (iMail.indexOf("@") > 0) {
                                regEMail = "";
                            }
                            TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signup(iMail, iPassword, iMail, iMail, iMail + regEMail, new Callback<UserSignup>() {
                                @Override
                                public void success(UserSignup user, Response response) {
                                    if (null != user.error) {
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                        String errorStr = android.text.Html.fromHtml(user.error).toString();
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        TextView messageText = new TextView(getActivity());
                                        messageText.setGravity(Gravity.CENTER);
                                        messageText.setTextSize(22);

                                        messageText.setText(errorStr);

                                        builder.setView(messageText);
                                        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).setTitle(getActivity().getString(R.string.server_title));
                                        builder.setCancelable(false);
                                        builder.show();


                                        showToast(user.error);
                                    } else {
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                        if (isActive) {
                                            Log.d("TAG", user.toString());
//                                        showToast("LOGIN " + user.fullname + " " + user.last_name);

                                            TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signin(user.user_login, iPassword, new Callback<User>() {
                                                @Override
                                                public void success(User user, Response response) {
                                                    onSuccessLogin(user);
                                                }

                                                @Override
                                                public void failure(RetrofitError error) {
                                                    onFailureLogin(error);
                                                }
                                            });


//                                        mSharedPreference.setUserID(String.valueOf(user.user_id));
//
//                                        Intent startHomeActivity = new Intent(mContext, MainTabActivity.class);
//                                        startActivity(startHomeActivity);
//                                        getActivity().finish();
                                        }
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    mProgressBar.setVisibility(View.INVISIBLE);
                                    if (isActive) {
                                        Log.d("TAG", error.toString());
                                        showToast(" Please make sure you fill in a correct email and password");
                                    }
                                }
                            });
                        } else {
                            showToast("Please fill in your user e-mail and password");

                        }

//                mProgressIndicator.setVisibility(View.VISIBLE);
//                mButtonLogin.setVisibility(View.INVISIBLE);
//                authorize();
                } else {
                    showToast("Please Read and Agree to our Terms & Conditions");
                }
                } else {
                    showToast(getString(R.string.network_error));
                }
            }
        });




//        mProgressIndicator = (ProgressBar) view.findViewById(R.id.progress_indicator);

        return view;

    }


    @Override
    public void onPause() {
        super.onPause();

        isActive = false;
    }


	@Override
    public void onResume() {
        super.onResume();

        isActive = true;

    }

    private void onSuccessLogin(User user){
        if(isActive) {
            Log.d(TAG, user.toString());
            mProgressBar.setVisibility(View.INVISIBLE);
            mSharedPreference.setUserID(String.valueOf(user.id));
            mSharedPreference.setAuthKey(user.auth.key);

            Intent startHomeActivity = new Intent(mContext, MainTabActivity.class);
            startActivity(startHomeActivity);
            getActivity().finish();
        }
    }

    private void onFailureLogin(RetrofitError error){
        if(isActive) {
            Log.d(TAG, error.toString());
            if (error.toString().equals("404 Not Found")) {
                showToast("Please make sure you fill in a correct email and password");
            } else {
                showToast("Server Unavailable, Please try again later");
            }
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }


    private void showNameToast(String name) {
        showToast("You are logged in as\n" + name);
    }

    private void showToast(String message) {
            Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
    }



}
