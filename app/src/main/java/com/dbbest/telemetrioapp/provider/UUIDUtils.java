package com.dbbest.telemetrioapp.provider;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class UUIDUtils {

	private static final String HASH_STANDARD = "MD5";

	public static final String generateUniqueID(Context context) {
		String result = null;

		try {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			result = telephonyManager.getDeviceId();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (result != null)
			return result;

		try {
			result = Secure.getString(context.getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (result == null) {
			result = "355" + Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10
					+ Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
					+ Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
					+ Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10;
//					+ Build.USER.length() % 10;
		}

		result = result + Build.TIME;

		MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_STANDARD);
			byte[] messageDigest = md.digest(result.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			result = number.toString(16);

			while (result.length() < 32) {
				result = "0" + result;
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return result;
	}

}
