package com.dbbest.telemetrioapp.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.EventDetailActivity;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.service.UpdateService;
import com.dbbest.telemetrioapp.view.adapter.ScheduleListAdapter;


public class TabScheduleFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = TabScheduleFragment.class.getSimpleName();
    private static final int URL_EVENT_LOADER = 0;

    private Context mContext;
    private FragmentActivityBackStack stack;
    private SharedPreference mSharedPreference;

    private String requestId;
    private BroadcastReceiver requestReceiver;
    private boolean isActive;
    private ProgressBar mProgressBar;

    private ListView mListView;
    private ScheduleListAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private InnerResultReceiver mResultReceiver;

    private String mUserId;



    public static TabScheduleFragment newInstance() {
        TabScheduleFragment fragment = new TabScheduleFragment();
        fragment.stack = null;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity();

        mSharedPreference = new SharedPreference(mContext);

        mUserId = mSharedPreference.getUserID();

        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_schedule, container, false);
        mListView= (ListView) view.findViewById(R.id.listViewEvents);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mRefreshLayout.setOnRefreshListener(this);


//        //FIXME: remove this button from layout, it's only for testing auto-update UI
//        view.findViewById(R.id.button_clean).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getContentResolver().delete(TelemetrioContract.Events.CONTENT_URI, null, null);
//            }
//        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new ScheduleListAdapter(mContext, null);
        mListView.setAdapter(mAdapter);
        mResultReceiver = new InnerResultReceiver(new Handler());


        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(URL_EVENT_LOADER, null, this);

        onRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
        mResultReceiver.setReceiverListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
        mResultReceiver.setReceiverListener(null);
    }



    private void showToast(String message) {
            Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
    }

    // These are the Contacts rows that we will retrieve.
    static final String[] EVENTS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Events._ID,
            TelemetrioContract.Events.EVENT_LEAGUE_NAME,
            TelemetrioContract.Events.EVENT_FIELD_NAME,
            TelemetrioContract.Events.EVENT_TIME_BEGIN,
            TelemetrioContract.Events.EVENT_TIME_END,
            TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME,
            TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME,
            TelemetrioContract.Events.EVENT_FIELD_ID,
            TelemetrioContract.Events.EVENT_ID_GAME,

            TelemetrioContract.Events.EVENT_TYPE,
            TelemetrioContract.Events.EVENT_TODAY,

    };


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.

        if (id == URL_EVENT_LOADER) {
            Uri baseUri = TelemetrioContract.Events.CONTENT_URI;

            return new CursorLoader(getActivity(), baseUri,
                    EVENTS_SUMMARY_PROJECTION, null, null,
                    TelemetrioContract.Events.DEFAULT_SORT);
        }
        else{
            return null;
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        mAdapter.swapCursor(data);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                Intent eventDetail = new Intent(getActivity(), EventDetailActivity.class);
                eventDetail.putExtra("position", position);
                startActivity(eventDetail);
            }
        });

    }

    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }

    @Override
    public void onRefresh() {
        if (null != mUserId) {
            UpdateService.startActionFetchNewEvents(getActivity(), mUserId, mResultReceiver);
            UpdateService.startActionFetchConfigurations(getActivity(), mUserId, mResultReceiver);
        }
    }

    private static class InnerResultReceiver extends ResultReceiver {
        private TabScheduleFragment fragment;

        public InnerResultReceiver(Handler handler) {
            super(handler);
        }

        public void setReceiverListener(TabScheduleFragment fragment) {
            this.fragment = fragment;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (fragment != null) {
                fragment.mRefreshLayout.setRefreshing(false);
                if (resultCode == Activity.RESULT_CANCELED){
                    Toast.makeText(fragment.getActivity(), "Update error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Log.w("TAG", "Can't find fragment");
            }
        }
    }
}
