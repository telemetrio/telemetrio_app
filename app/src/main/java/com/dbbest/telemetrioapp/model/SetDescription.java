package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;


public class SetDescription {

    @SerializedName("local_id")
    public long local_id;
    @SerializedName("description")
    public String description;


    @Override
    public String toString() {
        return "SetDescription{" +
                "local_id=" + local_id +
                ", description=" + description +
                '}';
    }
}
