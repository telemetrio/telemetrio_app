package com.dbbest.telemetrioapp.model;

import java.util.ArrayList;

/**
 * Created by Andrew Druk on 11/26/14.
 */
public class User {

    public long id;
    public String user_login;
    public String user_email;
    public String first_name;
    public String last_name;
    public String photo;
    public long idCoupon;
    public String coupon_number;
    public String expiration;
    public String registration_data;
    public long idSubscription;
    public long state;
    public long idPlayer;
    public String registered_email;
    public String name_player;
    public String email_state;
    public String subscription_date;
    public String expiration_date;
    public long idPayment;

    public ArrayList<Team> team_list;
    public Auth auth;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", user_login='" + user_login + '\'' +
                ", user_email='" + user_email + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", photo='" + photo + '\'' +
                ", idCoupon=" + idCoupon +
                ", coupon_number='" + coupon_number + '\'' +
                ", expiration='" + expiration + '\'' +
                ", registration_data='" + registration_data + '\'' +
                ", idSubscription=" + idSubscription +
                ", state=" + state +
                ", idPlayer=" + idPlayer +
                ", registered_email='" + registered_email + '\'' +
                ", name_player='" + name_player + '\'' +
                ", email_state='" + email_state + '\'' +
                ", subscription_date='" + subscription_date + '\'' +
                ", expiration_date='" + expiration_date + '\'' +
                ", idPayment=" + idPayment +
                ", team_list=" + team_list +
                '}';
    }
}
