/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dbbest.telemetrioapp.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;

import java.io.IOException;

public class GooglePlusBaseActivity extends FragmentActivity implements PlusClient.ConnectionCallbacks, PlusClient.OnConnectionFailedListener, PlusClient.OnAccessRevokedListener {

    private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;

    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;

    protected PlusClient mPlusClient;
    protected ConnectionResult mConnectionResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    protected void init(){
        mPlusClient = new PlusClient.Builder(this, this, this)
                .setScopes("https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email")
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPlusClient.connect();
    }

    @Override
    public void onStop() {
        mPlusClient.disconnect();
        super.onStop();
    }


    public void signIn(){
        int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (available != ConnectionResult.SUCCESS) {
            showDialog(DIALOG_GET_GOOGLE_PLAY_SERVICES);
            return;
        }

        try {
            if(mConnectionResult != null){
                mConnectionResult.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
            }
            else {
                tryConnectClient();
            }
        } catch (IntentSender.SendIntentException e) {
            mPlusClient.connect();
        }
    }

    public void signOut(){
        if (mPlusClient.isConnected()) {
            mPlusClient.clearDefaultAccount();
            mPlusClient.disconnect();
            mPlusClient.connect();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id != DIALOG_GET_GOOGLE_PLAY_SERVICES) {
            return super.onCreateDialog(id);
        }

        int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (available == ConnectionResult.SUCCESS) {
            return null;
        }
        if (GooglePlayServicesUtil.isUserRecoverableError(available)) {
            return GooglePlayServicesUtil.getErrorDialog(
                    available, this, REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES);
        }
        return new AlertDialog.Builder(this)
                .setMessage("Sign in with Google is not available.")
                .setCancelable(true)
                .create();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SIGN_IN || requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
            if (resultCode == RESULT_OK) {
                tryConnectClient();
            }
//            else cancelProgressDialog();
        }
    }

    @Override
    public void onAccessRevoked(ConnectionResult status) {
        if (status.isSuccess()) {

        } else {
            mPlusClient.disconnect();
        }
        mPlusClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        new GetAccessTokenTask().execute();
    }

    protected void onLoggedInGoogle(String token) {
        signOut();
    }

    @Override
    public void onDisconnected() {
        mPlusClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mConnectionResult = result;
    }


    private void tryConnectClient(){
        if (!mPlusClient.isConnected() && !mPlusClient.isConnecting()) {
            mPlusClient.connect();
        }
    }

    class GetAccessTokenTask extends AsyncTask<Void, String, String> {
        private static final String TAG = "GetAccessTokenTask";

        @Override
        protected String doInBackground(Void... params) {
            String token = null;
            String scope = "oauth2:" + "https://www.googleapis.com/auth/userinfo.profile" + " https://www.googleapis.com/auth/userinfo.email";
            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), mPlusClient.getAccountName(), scope);
                Log.d(TAG, token);
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES);
                e.printStackTrace();
                Log.d(TAG, e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, e.getMessage());
            } catch (GoogleAuthException e) {
                e.printStackTrace();
                Log.d(TAG, e.getMessage());
            }

            return token;
        }

        @Override
        protected void onPostExecute(String token) {
            onLoggedInGoogle(token);
        }
    };

}
