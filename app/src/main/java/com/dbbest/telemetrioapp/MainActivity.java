package com.dbbest.telemetrioapp;


import android.content.Intent;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dbbest.telemetrioapp.activity.GooglePlusBaseActivity;
import com.dbbest.telemetrioapp.activity.LoginActivity;
import com.dbbest.telemetrioapp.activity.SplashActivity;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;

import com.dbbest.telemetrioapp.model.RegisterDevice;
import com.dbbest.telemetrioapp.provider.UUIDUtils;
import com.dbbest.telemetrioapp.service.AlarmManagerBroadcastReceiver;
import com.dbbest.telemetrioapp.util.SocialUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.plus.model.people.Person;

import com.dbbest.telemetrioapp.controller.SharedPreference;

import java.io.IOException;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


public class MainActivity extends GooglePlusBaseActivity implements FragmentActivityBackStack {

//    private AuthorizationManager mOAuthManager;
//    private Button mButtonLogin;
    private ProgressBar mProgressIndicator;
    private GPSTracker gps;
    private double lat = 0;
    private double lon = 0;
    private SharedPreference sharedPreference;

    String SENDER_ID = "296049960769";
    GoogleCloudMessaging gcm;
    String regid;
    String deviceId;
    private AlarmManagerBroadcastReceiver alarm;
    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
//        mOAuthManager = AuthorizationManager.getInstance(this);
        setContentView(R.layout.main_activity);

        sharedPreference = new SharedPreference(getBaseContext());

        userId = sharedPreference.getUserID();

        new registerInBackground().execute("");

        gps = new GPSTracker(MainActivity.this);

        // check if GPS enabled
        if (gps.canGetLocation() && null != gps.getLocation()) {

            Location location = gps.getLocation();

            lat = location.getLatitude();
            lon = location.getLongitude();

            sharedPreference.setLatLon(lat, lon);

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            if (0 == latitude && 0 == longitude) {
                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Criteria criteria = new Criteria();
                String provider = lm.getBestProvider(criteria, true);
                Location newLocation = lm.getLastKnownLocation(provider);

                if (null != newLocation) {
                    lat = newLocation.getLatitude();
                    lon = newLocation.getLongitude();
                    sharedPreference.setLatLon(lat, lon);
                }
            }
        } else {

            gps.showSettingsAlert();
        }


        alarm = new AlarmManagerBroadcastReceiver();
        Context context = this.getApplicationContext();
        if(alarm != null){
            alarm.SetAlarm(context);
        }else{
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
        pushFragmentToBackStack(SplashActivity.newInstance(), false);

    }


    @Override
    public void pushFragmentToBackStack(Fragment fragment, Boolean withAnimation) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (withAnimation) {
            transaction.setCustomAnimations(R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left, R.anim.slide_in_from_left,
                    R.anim.slide_out_to_right);
        }
        transaction.replace(R.id.content, fragment);
        transaction.addToBackStack("NOROOT");
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
            overridePendingTransition(R.anim.slide_in_from_left,
                    R.anim.slide_out_to_right);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SocialUtils.Facebook.FACEBOOK_REQUEST && resultCode == RESULT_OK){
            SocialUtils.Facebook.onFacebookActivityResult(this, requestCode, resultCode, data);
        }
    }

    @Override
    protected void onLoggedInGoogle(final String token) {
        String email = mPlusClient.getAccountName();
        Person person = mPlusClient.getCurrentPerson();
        String userId = null;
        String username = null;
        if(person != null){
            userId = person.getId();
            username = person.getDisplayName();
            Log.d("RequestTAG-GoogleAuth", "Email: " + email + " Id: " + userId);
        }

        Intent intent = new Intent(LoginActivity.ACTION_LOGIN);
        intent.putExtra(LoginActivity.USER_ID, userId);
        intent.putExtra(LoginActivity.USER_NAME, username);
        intent.putExtra(LoginActivity.EMAIL, email);
        intent.putExtra(LoginActivity.FIRST_NAME, username);
        intent.putExtra(LoginActivity.LAST_NAME, username);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        super.onLoggedInGoogle(token);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static void startInNewTask(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    private class registerInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String msg = "";
            try {
                deviceId = getUUID(getBaseContext());

                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                }
                regid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regid;
                Log.d("GCM", regid);


                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                sendRegistrationIdToBackend(regid, deviceId);

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
//                    storeRegistrationId(context, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }

        private void sendRegistrationIdToBackend(String regid, String deviceId) {
            if (null != userId && !userId.equals("")) {
                TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).registerDevice(userId, regid, deviceId, new Callback<RegisterDevice>() {
                    @Override
                    public void success(RegisterDevice serverResp, Response response) {
                        if (null != serverResp) {

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("", error.toString());
                    }

                });
            }
        }

        @Override
        protected void onPostExecute(String result) {

////                mDisplay.append(msg + "\n");
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }


    public String getUUID(Context context) {
        if (null == deviceId) {
            deviceId = UUIDUtils.generateUniqueID(context);
        }
        return deviceId;
    }

}
