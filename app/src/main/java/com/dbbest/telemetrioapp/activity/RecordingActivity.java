package com.dbbest.telemetrioapp.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.recording.RecordManager;
import com.dbbest.telemetrioapp.recording.SavePhotoAsync;

import java.io.File;
import java.util.List;

/**
 * Created by Alexander Sokol on 11/25/2014-11:48 AM
 * for project Telemetrio.
 */
public class RecordingActivity extends Activity implements View.OnClickListener,
        RecordManager.OnRecordStatesListener,
        LoaderManager.LoaderCallbacks<String> {

    public static final String KEY_SAVE_PATH = "key_save_path";
    public static final String KEY_START_WITH_PHOTO = "key_start_with_photo";

    public static final String KEY_EVENT_ID = "key_event_id";

    public static final String KEY_IS_VIDEO = "key_is_video";
    public static final String KEY_RESULT_FILE_PATH = "key_result_file";

    private static final String PHOTO_EXTENSION = ".jpg";
    private static final String VIDEO_EXTENSION = ".mp4";

    private ImageButton          mBtnChangeMode;
    private ImageButton          mBtnBookmark;
    private ImageButton     mBtnRecord;

    private ProgressBar     mPbProgress;
    private View            mWaitView;

    private String          mSavePhotoPath;

    private RecordManager   mRecordManager;
    private ProgressUpdater mProgressUpdater;
    private Intent          mResultData;
    private byte[]          mPictureData;

    private Mode            mMode;

    enum Mode{
        PHOTO,
        VIDEO
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String saveDir = getIntent().getStringExtra(KEY_SAVE_PATH);
        String mSaveVideoPath;
        if(TextUtils.isEmpty(saveDir)){
            throw new IllegalArgumentException("Save path is empty!");
        }
        else{
            File saveDirFile = new File(saveDir);
            if(saveDirFile.exists()){
                if(saveDirFile.isFile())
                    throw new IllegalArgumentException("Save path must be a directory");
            }
            else{
                saveDirFile.mkdirs();
            }
            mSavePhotoPath = saveDir + File.separator + System.currentTimeMillis() + PHOTO_EXTENSION;
            mSaveVideoPath = saveDir + File.separator + System.currentTimeMillis() + VIDEO_EXTENSION;

        }

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(getActionBar() != null)
            getActionBar().hide();

        setContentView(R.layout.activity_recording);


        SurfaceView preview = (SurfaceView) findViewById(R.id.surfaceView);
        mRecordManager = new RecordManager(preview.getHolder(), mSaveVideoPath, this);
        mRecordManager.setOrientation(90);
        mBtnChangeMode = (ImageButton) findViewById(R.id.btn_change_mode);
        mBtnBookmark = (ImageButton) findViewById(R.id.btn_bookmark);
        Button mBtnDone = (Button) findViewById(R.id.btn_done);
        Button mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnRecord = (ImageButton) findViewById(R.id.btn_record);
        mPbProgress = (ProgressBar) findViewById(R.id.progressBar);
        mWaitView = findViewById(R.id.progress_layout);

        mBtnDone.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnRecord.setOnClickListener(this);
        mBtnChangeMode.setOnClickListener(this);
        mBtnBookmark.setOnClickListener(this);

        mPbProgress.setMax(15000);
        mProgressUpdater = new ProgressUpdater();

        if (getIntent().getBooleanExtra(KEY_START_WITH_PHOTO, true)){
            mMode = Mode.PHOTO;
//            mBtnChangeMode.setText("Video");
            mBtnChangeMode.setImageResource(R.drawable.icon_video_rec_video);
            mPbProgress.setVisibility(View.INVISIBLE);
        } else{
            mMode = Mode.VIDEO;
//            mBtnChangeMode.setText("Photo");
            mBtnChangeMode.setImageResource(R.drawable.icon_camera_recording);
            mPbProgress.setVisibility(View.VISIBLE);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        mRecordManager.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mRecordManager.onStop();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_change_mode : {
                if (mMode == Mode.PHOTO){
                    mMode = Mode.VIDEO;
//                    mBtnChangeMode.setText("Photo");
                    mBtnChangeMode.setImageResource(R.drawable.icon_camera_recording);
                    mPbProgress.setVisibility(View.VISIBLE);
                }
                else if (mMode == Mode.VIDEO){
                    mMode = Mode.PHOTO;
//                    mBtnChangeMode.setText("Video");
                    mBtnChangeMode.setImageResource(R.drawable.icon_video_rec_video);
                    mPbProgress.setVisibility(View.INVISIBLE);
                }
                break;
            }
            case R.id.btn_bookmark :{
                Intent bookmarkActivity = new Intent(RecordingActivity.this, BookmarkerActivity.class);
//                bookmarkActivity.putExtra("position", mPosition);
                bookmarkActivity.putExtra("event_id", getIntent().getStringExtra("event_id"));
                startActivity(bookmarkActivity);
                break;
            }

            case R.id.btn_done : {
                if(mResultData != null){
                    mRecordManager.closeCamera(new RecordManager.OnCameraReleaseListener() {
                        @Override
                        public void onCameraReleased() {
                            setResult(RESULT_OK, mResultData);
                            finish();
                        }
                    });
                }
                break;
            }
            case R.id.btn_cancel : {
                mPbProgress.setProgress(0);
                final View view = findViewById(R.id.confirmation_layout);
                mRecordManager.startPreviewAsync();
                ValueAnimator fadeOutProgressAnim = ObjectAnimator.ofFloat(mPbProgress, "alpha", 0f, 1f);
                ValueAnimator fadeOutAnim = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f);
                ValueAnimator fadeInAnim  = ObjectAnimator.ofFloat(mBtnRecord, "alpha", 0f, 1f);
                fadeInAnim.setDuration(250);
                fadeOutAnim.setDuration(250);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(fadeInAnim).with(fadeOutAnim).with(fadeOutProgressAnim);
                animatorSet.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animatorSet.start();
                break;
            }
            case R.id.btn_record : {
                if (mMode == Mode.PHOTO){
                    mRecordManager.takePhoto(false);
                }
                else if (mMode == Mode.VIDEO){
                    int cProgress = mPbProgress.getProgress();
                    if ((5000 <  cProgress) || (0 == cProgress)) {
                        if (mRecordManager.isRecordingStarted()) {
                            mRecordManager.stopRecording();
                        } else {
                            mRecordManager.startRecording(false);
                        }
                    }

                }
                break;
            }
        }
    }


    @Override
    public void onCameraFailed() {
        Toast.makeText(this, "Camera Failed", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRecordingStarted() {
        mProgressUpdater.start();
    }


    @Override
    public void onRecordingComplete(String path) {
        setResultData(true, path);
        mProgressUpdater.stop();
        mWaitView.setVisibility(View.GONE);

        View view = findViewById(R.id.confirmation_layout);
        view.setVisibility(View.VISIBLE);
        ValueAnimator fadeOutProgressAnim = ObjectAnimator.ofFloat(mPbProgress, "alpha", 1f, 0f);
        ValueAnimator fadeOutAnim = ObjectAnimator.ofFloat(mBtnRecord, "alpha", 1f, 0f);
        ValueAnimator fadeInAnim  = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
        fadeInAnim.setDuration(250);
        fadeOutAnim.setDuration(250);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(fadeInAnim).with(fadeOutAnim).with(fadeOutProgressAnim);
        animatorSet.start();
    }

    @Override
    public void onRecordingFailed() {
        Toast.makeText(this, "Video recording failed", Toast.LENGTH_SHORT).show();
        mProgressUpdater.stop();
        mPbProgress.setProgress(0);
        mWaitView.setVisibility(View.GONE);
    }


    @Override
    public void onPreviewStarted() {

    }

    @Override
    public void onPhotoTaken(byte[] data) {
        mPictureData = data;
        mWaitView.setVisibility(View.VISIBLE);
        getLoaderManager().restartLoader(1, Bundle.EMPTY, this);
    }


    @Override
    public void onPhotoFailed() {
        Toast.makeText(this, "Failed to take a photo", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onCameraOpened(Camera camera) {
        List<Camera.Size> supportedVideoSizes = camera.getParameters().getSupportedVideoSizes();
        if (null != supportedVideoSizes) {
        int width = 0;
        int height = 0;
            for (Camera.Size size : supportedVideoSizes) {
                if (width < size.width && height < size.height) {
                    width = size.width;
                    height = size.height;
                }
            }
        mRecordManager.setVideoSize(width, height);
        }
    }


    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        Loader<String> savePhoto = new SavePhotoAsync(this, mPictureData, mSavePhotoPath, mRecordManager.getRotation());
        savePhoto.forceLoad();
        return savePhoto;
    }


    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        mPictureData = null;
        setResultData(false, data);
        mWaitView.setVisibility(View.GONE);

        View view = findViewById(R.id.confirmation_layout);
        view.setVisibility(View.VISIBLE);
        ValueAnimator fadeOutAnim = ObjectAnimator.ofFloat(mBtnRecord, "alpha", 1f, 0f);
        ValueAnimator fadeInAnim  = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
        fadeInAnim.setDuration(250);
        fadeOutAnim.setDuration(250);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(fadeInAnim).with(fadeOutAnim);
        animatorSet.start();
    }


    @Override
    public void onLoaderReset(Loader<String> loader) {
    }


    private void setResultData(boolean isVideo, String path){
        mResultData = new Intent();
        mResultData.putExtra(KEY_IS_VIDEO, isVideo);
        mResultData.putExtra(KEY_RESULT_FILE_PATH, path);
    }

    public static void start(Activity activity, String filePath, String eventId, int requestCode) {
        Intent intent = new Intent(activity, RecordingActivity.class);
        intent.putExtra(RecordingActivity.KEY_SAVE_PATH, filePath);
        intent.putExtra(KEY_EVENT_ID, eventId);
        activity.startActivityForResult(intent, requestCode);
    }


    private class ProgressUpdater{

        private long lastUpdate = 0;

        private Handler updateHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                long upd = System.currentTimeMillis() - lastUpdate;
                mPbProgress.setProgress((int) (mPbProgress.getProgress() + upd));

                int cProgress = mPbProgress.getProgress();
                if (cProgress > 14999) {
                    if (mRecordManager.isRecordingStarted()) {
                        mBtnRecord.callOnClick();
                    }
                }
                lastUpdate = System.currentTimeMillis();
            }
        };

        private Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                updateHandler.sendEmptyMessage(1);
                updateHandler.postDelayed(updateRunnable, 100);
            }
        };

        public void start(){
            lastUpdate = System.currentTimeMillis();
            updateHandler.post(updateRunnable);
        }


        public void stop(){
            updateHandler.removeCallbacks(updateRunnable);
//            if (mRecordManager.isRecordingStarted()) {
//                mBtnRecord.callOnClick();
//            }
        }
    }
}
