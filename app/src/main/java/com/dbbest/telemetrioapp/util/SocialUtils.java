package com.dbbest.telemetrioapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.facebook.Session;

public class SocialUtils {

    public static class Facebook{

        public static final int FACEBOOK_REQUEST = 64206;

        public static void onFacebookActivityResult(Activity activity, int requestCode, int resultCode, Intent data){
            if(data != null){
                Session.getActiveSession().onActivityResult(activity, requestCode, resultCode, data);
            }
        }

        public static void clearFacebookSession(Context context) {
            Session session = Session.getActiveSession();
            if (session != null) {
                if (!session.isClosed()) {
                    session.closeAndClearTokenInformation();
                    //clear your preferences if saved
                }
            } else {
                session = new Session(context);
                Session.setActiveSession(session);
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        }

    }

    public static class GooglePlus{

    }


}
