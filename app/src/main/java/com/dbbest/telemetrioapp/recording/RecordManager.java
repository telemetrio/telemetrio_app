package com.dbbest.telemetrioapp.recording;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.File;
import java.io.IOException;

/**
 * ]
 * Created by Alexander Sokol on 11/25/2014-11:55 AM
 * for project SimpleCamera.
 */
public class RecordManager {

    private static final String TAG = "RecordManager";

    private Camera                  mCamera;
    private boolean                 mIsCameraOpened;
    private boolean                 mIsSurfaceCreated;
    private boolean                 mRecordingStarted;
    private boolean                 mIsPreviewStarted;
    private SurfaceHolder           mSurfaceHolder;
    private MediaRecorder           mRecorder;
    private volatile int            mCameraRotation;
    private volatile int                     mMaxRecordingDuration = 15000;
    private final                   String mSavePath;
    private volatile int            mVideoWidth = 0;
    private volatile int            mVideoHeight = 0;

    private EventHandler            mEventHandler;
    private HandlerThread           mRecorderThread;
    private Handler                 mRecorderThreadHandler;
    private final Object            mMonitor;


    public RecordManager(SurfaceHolder surfaceHolder, String savePath, OnRecordStatesListener listener){

        mMonitor = new Object();
        mEventHandler = new EventHandler(listener);

        mRecorderThread = new HandlerThread("RecorderThread"){
            @Override
            protected void onLooperPrepared() {
                super.onLooperPrepared();
                mRecorderThreadHandler = new Handler(Looper.myLooper());
                synchronized (mMonitor){
                    mMonitor.notify();
                }
            }
        };
        mRecorderThread.start();

        synchronized (mMonitor){
            try{
                mMonitor.wait();
            } catch (InterruptedException ex){
                ex.printStackTrace();
            }
        }

        mSavePath = savePath;
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(final SurfaceHolder holder) {
                mRecorderThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mIsSurfaceCreated = true;
                        mSurfaceHolder = holder;

                        openCamera();
                        startPreview();
                    }
                });
            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(final SurfaceHolder holder) {
                mRecorderThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        closeCamera();
                        mIsSurfaceCreated = false;
                    }
                });
            }
        });
    }


    /**
     * Opens camera in render thread
     */
    private void openCamera(){
        try {
            mIsCameraOpened = true;
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(mCameraRotation);
            mEventHandler.onCameraOpened(mCamera);
        } catch (Exception ex){
            Log.e(TAG, "Failed to connect to camera service");
            mEventHandler.onCameraFailed();
        }
    }


    /**
     * Set camera display orientation and recording orientation
     * @param angle angle to rotate image clock wise
     */
    public void setOrientation(final int angle){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsCameraOpened) {
                    mCamera.setDisplayOrientation(angle);
                }
                mCameraRotation = angle;
            }
        });
    }


    /**
     * Set recording duration, 0 - infinite duration
     * @param duration recording duration im milliseconds
     */
    public void setRecordingDuration(final int duration){
        mMaxRecordingDuration = duration;
    }


    /**
     * Set photo image size
     * @param width photo width
     * @param height photo height
     */
    public void setPhotoSize(final int width, final int height){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsCameraOpened){
                    try {
                        Camera.Parameters params = mCamera.getParameters();
                        params.setPictureSize(width, height);
                        mCamera.setParameters(params);
                    }catch(Exception ex){
                        Log.e(TAG, "Failed to set photo size");
                    }
                }
            }
        });
    }


    /**
     * Start preview from camera if camera is opened
     * in render thread
     */
    private void startPreview(){
        if(mIsCameraOpened && !mIsPreviewStarted){
            if(null != mCamera && null != mSurfaceHolder) {
                try {
                    mCamera.setPreviewDisplay(mSurfaceHolder);
                    mCamera.startPreview();
                    mIsPreviewStarted = true;
                    mEventHandler.onPreviewStarted();
                } catch (IOException e) {
                    Log.e(TAG, "Failed set preview display");
                    mEventHandler.onCameraFailed();
                }
            }
        }
    }


    /**
     * Starts preview from camera if camera is opened
     * async
     */
    public void startPreviewAsync(){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                startPreview();
            }
        });
    }


    /**
     * Starts preview from camera if camera is opened
     * in render thread
     */
    private void stopPreview(){
        if(mIsPreviewStarted && mIsCameraOpened){
            mCamera.stopPreview();
            mIsPreviewStarted = false;
        }
    }

    /**
     * Starts video recording
     * @param autoFocus true if auto-focus needed
     */
    public void startRecording(final boolean autoFocus){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsCameraOpened){
                    if(autoFocus){
                        mCamera.autoFocus(new Camera.AutoFocusCallback() {
                            @Override
                            public void onAutoFocus(boolean success, Camera camera) {
                                startRecording();
                            }
                        });
                    }
                    else{
                        startRecording();
                    }
                }
                else{
                    Log.e(TAG, "Failed to start recording: camera was not opened");
                }
            }
        });
    }


    /**
     * Starts video recording
     * in render thread
     */
    private void startRecording(){
        if(mIsSurfaceCreated) {
            try {
                mRecorder = new MediaRecorder();
                mCamera.unlock();
                mRecorder.setCamera(mCamera);

                mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mRecorder.setOrientationHint(mCameraRotation);
                mRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());
                mRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
                mRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());
                mRecorder.setOutputFile(mSavePath);
                mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {

                    }
                });

                if(mVideoWidth != 0 && mVideoHeight != 0){
                    mRecorder.setVideoSize(mVideoWidth, mVideoHeight);
                }

                if(mMaxRecordingDuration < 0)
                    mRecorder.setMaxDuration(mMaxRecordingDuration);

                mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {
                        Log.d(TAG, "on recorder info " + what);
                        if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED){
                            stopPreview();
                            mEventHandler.onRecordingComplete(mSavePath);
                        }
                    }
                });
                mRecorder.prepare();
                mRecorder.start();
                mEventHandler.onRecordingStarted();
                mRecordingStarted = true;

            } catch (Exception ex) {
                Log.e(TAG, "Failed to start recording");
            }
        }
        else{
            Log.e(TAG, "Failed to start recording: surface was not created");
        }
    }


    public boolean isRecordingStarted(){
        return mRecordingStarted;
    }


    /**
     * Stops video recording
     */
    public void stopRecording(){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mRecordingStarted) {
                    try {
                        mRecorder.stop();
                        mEventHandler.onRecordingComplete(mSavePath);
                        stopPreview();
                    }catch (Exception ex){
                        Log.e(TAG, "Failed to stop recording");
                        File file = new File(mSavePath);
                        if(file.exists())
                            file.delete();
                        mEventHandler.onRecordingFailed();
                    }
                    mRecorder.release();
                    mRecordingStarted = false;
                }
            }
        });
    }


    /**
     * Takes photo from camera
     * @param autoFocus true if auto-focus is needed
     */
    public void takePhoto(final boolean autoFocus){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsCameraOpened) {
                    try {
                        if (autoFocus) {
                            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                                @Override
                                public void onAutoFocus(boolean success, Camera camera) {
                                    Log.d(TAG, "Auto-focus result: " + success);
                                    takePhoto();
                                }
                            });
                        } else {
                            takePhoto();
                        }
                    } catch (Exception ex){
                        Log.e(TAG, "Failed to take photo");
                        mEventHandler.onPhotoFailed();
                    }
                }
                else{
                    mEventHandler.onPhotoFailed();
                }
            }
        });
    }


    /**
     * Takes photo
     * in render thread
     */
    private void takePhoto(){
        mCamera.takePicture(null, null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                stopPreview();
                mEventHandler.onPhotoTaken(data);
            }
        });
    }


    /**
     * Returns camera rotation
     * @return rotation degree clock wise
     */
    public int getRotation(){
        return mCameraRotation;
    }


    /**
     * Sets recording video resolution
     * @param width video width
     * @param height video height
     */
    public void setVideoSize(int width, int height){
        mVideoWidth = width;
        mVideoHeight = height;
    }


    /**
     * Close camera
     * in render thread
     */
    private void closeCamera(){
        if(mIsCameraOpened && (null != mCamera)) {
            mCamera.release();
        }

        mIsCameraOpened = false;
        mIsPreviewStarted = false;
        mEventHandler.onCameraReleased();
    }


    public void closeCamera(OnCameraReleaseListener listener){
        mEventHandler.setOnCameraReleaseListener(listener);
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCamera();
            }
        });
    }


    /**
     * according to activity lifecycle
     */
    public void onResume(){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsSurfaceCreated){
                    openCamera();
                    startPreview();
                }
            }
        });
    }

    /**
     * according to activity lifecycle
     */
    public void onStop(){
        mRecorderThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCamera();
            }
        });
    }

    public static interface OnCameraReleaseListener{
        void onCameraReleased();
    }

    public static interface OnRecordStatesListener{
        void onCameraFailed();
        void onRecordingStarted();
        void onRecordingComplete(String path);
        void onRecordingFailed();
        void onCameraOpened(Camera camera);
        void onPreviewStarted();
        void onPhotoTaken(byte[] data);
        void onPhotoFailed();
    }


    private class EventHandler implements OnRecordStatesListener, OnCameraReleaseListener{

        private Handler mHandler;
        private OnRecordStatesListener mListener;
        private OnCameraReleaseListener mReleaseListener;

        public EventHandler(OnRecordStatesListener listener){
            mHandler = new Handler(Looper.getMainLooper());
            mListener = listener;
        }


        public void setOnCameraReleaseListener(OnCameraReleaseListener listener){
            mReleaseListener = listener;
        }

        @Override
        public void onCameraReleased() {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(mReleaseListener != null)
                        mReleaseListener.onCameraReleased();
                }
            });
        }

        @Override
        public void onCameraFailed() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onCameraFailed();
                }
            });
        }

        @Override
        public void onRecordingStarted() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onRecordingStarted();
                }
            });
        }

        @Override
        public void onRecordingComplete(final String path) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onRecordingComplete(path);
                }
            });
        }

        @Override
        public void onRecordingFailed() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onRecordingFailed();
                }
            });
        }

        @Override
        public void onCameraOpened(final Camera camera) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onCameraOpened(camera);
                }
            });
        }

        @Override
        public void onPreviewStarted() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onPreviewStarted();
                }
            });
        }

        @Override
        public void onPhotoTaken(final byte[] data) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onPhotoTaken(data);
                }
            });
        }

        @Override
        public void onPhotoFailed() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onPhotoFailed();
                }
            });
        }
    }

}
