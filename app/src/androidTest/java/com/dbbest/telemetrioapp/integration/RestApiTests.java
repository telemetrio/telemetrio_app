package com.dbbest.telemetrioapp.integration;


import android.annotation.SuppressLint;
import android.content.Context;
import android.test.ApplicationTestCase;
import android.test.mock.MockContext;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.model.Event;
import com.dbbest.telemetrioapp.model.Photo;
import com.dbbest.telemetrioapp.model.SetBookmark;
import com.dbbest.telemetrioapp.model.User;
import com.dbbest.telemetrioapp.model.UserSignup;
import com.dbbest.telemetrioapp.util.Connectivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class RestApiTests extends TestCase{

    private static String sAuthKey;
    private static String sUserId;
    private static UserSignup lastRegisteredUser;
    private static String lastRegisteredUserPassword;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void test1_Registration(){
        final CountDownLatch signal = new CountDownLatch(1);

        String mail = UUID.randomUUID().toString() + "@test.com";
        final String password = UUID.randomUUID().toString();
        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signup(mail, password, mail, mail, mail, new Callback<UserSignup>() {
            @Override
            public void success(UserSignup userSignup, Response response) {
                assertNotNull(userSignup);
                lastRegisteredUser = userSignup;
                lastRegisteredUserPassword = password;
                Log.d("restClientTest", userSignup.toString());

                signal.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                if (!error.getCause().getMessage().contains("Unable to resolve host")) {
                    assertTrue(false);
                }

                signal.countDown();
            }
        });
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SmallTest
    public void test2_LastRegisteredUserLogin(){
        final CountDownLatch signal = new CountDownLatch(1);

        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signin(lastRegisteredUser.user_email, lastRegisteredUserPassword, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                assertNotNull(user);
                sAuthKey = user.auth.key;
                sUserId = String.valueOf(user.id);

                signal.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                if (!error.getCause().getMessage().contains("Unable to resolve host")){
                    assertTrue(false);
                }

                signal.countDown();
            }
        });
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SmallTest
    public void test3_CorrectLogin(){
        final CountDownLatch signal = new CountDownLatch(1);

        String mail = "marco_c";
        String password = "1234";
        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signin(mail, password, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                assertNotNull(user);
                sAuthKey = user.auth.key;
                sUserId = String.valueOf(user.id);

                signal.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                if (!error.getCause().getMessage().contains("Unable to resolve host")){
                    assertTrue(false);
                }

                signal.countDown();
            }
        });
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SmallTest
    public void test4_NotCorrectLogin(){
        final CountDownLatch signal = new CountDownLatch(1);

        String mail = "loremipsum";
        String password = "loremipsum";
        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signin(mail, password, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                assertTrue(false);
                signal.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                signal.countDown();
            }
        });
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SmallTest
    public void test5_Photos(){
        final CountDownLatch signal = new CountDownLatch(1);

        String userId = sUserId;
        try {
            List<Photo> photos = TelemetryApplication.getRestService(sAuthKey, userId, RestAdapter.LogLevel.BASIC).listPhotos(userId);
            assertNotNull(photos);

            signal.countDown();

            try {
                signal.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (Exception ignored){

        }
    }

    @SmallTest
    public void test6_Events(){
        final CountDownLatch signal = new CountDownLatch(1);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        try {
            List<Event> events = TelemetryApplication.getRestServiceWithConverter(sAuthKey, sUserId, RestAdapter.LogLevel.FULL, new GsonConverter(gson)).
                    listEvents(sUserId, "2");
            assertNotNull(events);

            signal.countDown();

            try {
                signal.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (Exception ignored){

        }
    }

    @SmallTest
    public void test7_Bookmarks(){
        final CountDownLatch signal = new CountDownLatch(1);

        TelemetryApplication.getRestService(sAuthKey, sUserId, RestAdapter.LogLevel.FULL).
                setBookmark(sUserId, "13", "1", 0, 0, new Callback<SetBookmark>() {
                    @Override
                    public void success(SetBookmark bookmark, Response response) {
                        assertNotNull(bookmark);
                        signal.countDown();
                    }


                    @Override
                    public void failure(RetrofitError error) {
                        signal.countDown();
                    }
                });

        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
