package com.dbbest.telemetrioapp.service;

public enum RequestOutcome {

	SUCCESS,
	FAILURE
}
