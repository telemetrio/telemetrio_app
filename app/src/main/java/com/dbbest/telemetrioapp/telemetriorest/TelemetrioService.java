package com.dbbest.telemetrioapp.telemetriorest;

import com.dbbest.telemetrioapp.model.Event;
import com.dbbest.telemetrioapp.model.GetConfiguration;
import com.dbbest.telemetrioapp.model.Photo;
import com.dbbest.telemetrioapp.model.RegisterDevice;
import com.dbbest.telemetrioapp.model.SetBookmark;
import com.dbbest.telemetrioapp.model.SetConfiguration;
import com.dbbest.telemetrioapp.model.SetDescription;
import com.dbbest.telemetrioapp.model.User;
import com.dbbest.telemetrioapp.model.UserSignup;
import com.dbbest.telemetrioapp.service.UploadService;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * Created by Andrew Druk on 11/25/14.
 */
public interface TelemetrioService {
    public static final String HEADER_API_KEY = "X-API-KEY";
    public static final String HEADER_USER_ID_KEY = "USER-ID";
    public static final String API_KEY = "ecf8cdbe7d082ed4494c3b74e3be3264d483565a";
    public static final String BASE_URL = "https://www.telemetrio.com/api";


    @FormUrlEncoded
    @POST("/access/signin")
    @Headers(HEADER_API_KEY + ": " + API_KEY)
    void signin(@Field("username") String username, @Field("password") String password, Callback<User> cb);

    @FormUrlEncoded
    @POST("/access/social")
    @Headers(HEADER_API_KEY + ": " + API_KEY)
    void loginSocial(@Field("user_social_id") String userId,
                     @Field("firstname") String firstName,
                     @Field("lastname") String lastName,
                     @Field("username") String userName,
                     @Field("email") String email,
                     Callback<User> cb);

    @FormUrlEncoded
    @POST("/access/signup")
    @Headers(HEADER_API_KEY + ": " + API_KEY)
    void signup(@Field("username") String username, @Field("password") String password,
                @Field("fullname") String fullname, @Field("lastname") String lastname,
                @Field("email") String email,
                Callback<UserSignup> cb);

    @FormUrlEncoded
    @POST("/events")
    List<Event> listEvents(@Field("user_id") String user, @Field("state") String state);

    @FormUrlEncoded
    @POST("/settings")
    void setConfiguration(@Field("user_id") String user,  @Field("setting") String setting,   @Field("state") String state, Callback<SetConfiguration> cb);

    @FormUrlEncoded
    @POST("/configuration")
    void getConfigurations(@Field("user_id") String user_id, Callback<GetConfiguration> callback);

    @FormUrlEncoded
    @POST("/photos")
    List<Photo> listPhotos(@Field("user_id") String user);


    @FormUrlEncoded
    @POST("/bookmark")
    void setBookmark(@Field("user_id") String user,
                     @Field("event_id") String event_id,
                     @Field("state") String action,
                     @Field("location_lat") double lat,
                     @Field("location_long") double lon,
                     Callback<SetBookmark> cb);

    @Multipart
    @POST("/upload")
    void upload(@Part("fileupload") TypedFile fileToUpload, @Part("user_id") TypedString id,
                @Part("local_id") TypedString localId,
                @Part("image") TypedString image,
                @Part("description") TypedString description,
                Callback<UploadService.UploadResponseEntity> cb);

    @FormUrlEncoded
    @POST("/deletemedia")
    void deleteMedia(@Field("multimedia_id") String multimedia_id, Callback<String[]> callback);

    @FormUrlEncoded
    @POST("/photo_description")
    void  setPhotoDescription(@Field("photo_id") String photo_id, @Field("description") String description,  Callback<SetDescription> callback);

    @FormUrlEncoded
    @POST("/register_device")
    void  registerDevice(@Field("user_id") String user_id, @Field("gcm_id") String gcm_id, @Field("device_id") String device_id,  Callback<RegisterDevice> callback);

}
