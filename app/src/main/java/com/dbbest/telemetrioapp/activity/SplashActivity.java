package com.dbbest.telemetrioapp.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;


public class SplashActivity extends Fragment {

    private static int SPLASH_TIME_OUT = 200;

    private FragmentActivityBackStack stack;
    private Context mContext;


    public SharedPreference sharedPreference;
    private String userID;

    public static SplashActivity newInstance() {
        SplashActivity fragment = new SplashActivity();
        fragment.stack = null;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity();

        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.splash_activity_layout, container,
                false);

        sharedPreference = new SharedPreference(mContext);
        userID = sharedPreference.getUserID();
        startTimeToFinishSplash();

        return view;
    }


    private void startTimeToFinishSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (null != userID) {

                    Intent startHomeActivity = new Intent(mContext, MainTabActivity.class);
                    startActivity(startHomeActivity);
                    getActivity().finish();

//                    stack.pushFragmentToBackStack(MainTabActivity.newInstance(), true);
                } else {
                    stack.pushFragmentToBackStack(LoginActivity.newInstance(), true);

                }
            }
        }, SPLASH_TIME_OUT);
    }

}
