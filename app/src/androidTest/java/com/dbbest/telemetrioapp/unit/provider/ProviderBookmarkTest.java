package com.dbbest.telemetrioapp.unit.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.dbbest.telemetrioapp.model.SetBookmark;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.provider.TelemetrioProvider;

import org.mockito.Mockito;

import java.util.Random;


public class ProviderBookmarkTest extends ProviderTestCase2<TelemetrioProvider> {

    public ProviderBookmarkTest() {
        super(TelemetrioProvider.class, TelemetrioContract.CONTENT_AUTHORITY);
    }

    private static final String[] BOOKMARKS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Bookmarks._ID,
            TelemetrioContract.Bookmarks.BOOKMARK_ID,
            TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID,
            TelemetrioContract.Bookmarks.BOOKMARK_ACTION,
            TelemetrioContract.Bookmarks.BOOKMARK_TIME,
            TelemetrioContract.Bookmarks.BOOKMARK_LAT,
            TelemetrioContract.Bookmarks.BOOKMARK_LONG
    };

    private Uri uri = TelemetrioContract.Bookmarks.CONTENT_URI;

    public void testInsert() {
        SetBookmark bookmark = createRandomBookmark();
        ContentValues values = fillContentValues(bookmark);
        Uri resultingUri = getMockContentResolver().insert(uri, values);

        assertNotNull(resultingUri);

        Cursor cursor = getCursorById(bookmark.event_id);
        compareDataInCursorAndInModel(cursor, bookmark);
    }

    public void testDelete() {
        SetBookmark bookmark = createRandomBookmark();
        ContentValues values = fillContentValues(bookmark);
        Uri resultingUri = getMockContentResolver().insert(uri, values);

        assertNotNull(resultingUri);

        String where = TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID + "=?";
        String[] args = {String.valueOf(bookmark.event_id)};
        int resultDelete = getMockContentResolver().delete(uri, where, args);

        assertTrue(resultDelete == 1);

        Cursor cursor = getCursorById(bookmark.event_id);
        assertTrue(cursor.getCount() == 0);
    }

    public void testUpdate() {
        SetBookmark bookmark = createRandomBookmark();
        ContentValues values = fillContentValues(bookmark);
        Uri resultingUri = getMockContentResolver().insert(uri, values);

        assertNotNull(resultingUri);

        SetBookmark newBookmark = createRandomBookmark();
        ContentValues valuesForUpdate = fillContentValues(newBookmark);

        String where = TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID + "=?";
        String[] args = {String.valueOf(bookmark.event_id)};
        int updateResult = getMockContentResolver().update(uri, valuesForUpdate, where, args);

        assertTrue(updateResult == 1);

        Cursor cursor = getCursorById(newBookmark.event_id);
        compareDataInCursorAndInModel(cursor, newBookmark);
    }

    private Cursor getCursorById(long id) {
        String WHERE =  TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID + "=?";
        String[] ARGS =  {String.valueOf(id)};
        return getMockContentResolver().query(uri, BOOKMARKS_SUMMARY_PROJECTION, WHERE, ARGS, TelemetrioContract.Bookmarks.DEFAULT_SORT);
    }

    private void compareDataInCursorAndInModel(Cursor cursor, SetBookmark bookmark) {
        while (cursor.moveToNext()) {
            long bookmarkId = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_ID));
            long action = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_ACTION));
            long time = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_TIME));
            long eventId = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID));
            long lat = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_LAT));
            long lon = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_LONG));

            assertTrue(time == bookmark.bookmark_time);
            assertTrue(bookmarkId == bookmark.bookmark_id);
            assertTrue(action == bookmark.bookmark_action);
            assertTrue(eventId == bookmark.event_id);
            assertTrue(lat == bookmark.bookmark_location_lat);
            assertTrue(lon == bookmark.bookmark_location_long);
        }
        cursor.close();
    }

    private ContentValues fillContentValues(SetBookmark bookmark) {
        ContentValues values = new ContentValues();
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_ID, bookmark.bookmark_id);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID, bookmark.event_id);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_ACTION, bookmark.bookmark_action);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_TIME, bookmark.bookmark_time);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_LAT, bookmark.bookmark_location_lat);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_LONG, bookmark.bookmark_location_long);
        return values;
    }

    private SetBookmark createRandomBookmark() {
        SetBookmark setBookmark = Mockito.mock(SetBookmark.class);
        setBookmark.event_id = new Random().nextInt();
        setBookmark.bookmark_id = new Random().nextInt();
        return setBookmark;
    }

}
