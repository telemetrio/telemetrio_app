package com.dbbest.telemetrioapp.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;


public class TabBookmarkFragment extends Fragment {

    private static final String TAG = TabBookmarkFragment.class.getSimpleName();


    private Context mContext;
    private FragmentActivityBackStack stack;
    private SharedPreference mSharedPreference;

    private String requestId;
    private BroadcastReceiver requestReceiver;
    private boolean isActive;
    private ProgressBar mProgressBar;


    public static TabBookmarkFragment newInstance() {
        TabBookmarkFragment fragment = new TabBookmarkFragment();
        fragment.stack = null;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity();

        mSharedPreference = new SharedPreference(mContext);
        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab_bookmark, container, false);




        return view;
    }





    @Override
    public void onPause() {
        super.onPause();

        isActive = false;
    }

	@Override
    public void onResume() {
        super.onResume();

        isActive = true;


    }




    private void showToast(String message) {
            Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
    }



}
