package com.dbbest.telemetrioapp.recording;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Alexander Sokol on 11/26/2014-12:03 PM
 * for project SimpleCamera.
 */
public class SavePhotoAsync extends AsyncTaskLoader<String> {

    private byte[] mData;
    private File mSaveFile;
    private int mRotation;

    public SavePhotoAsync(Context context, byte[] data, String savePath, int rotation) {
        super(context);
        mData = data;
        mSaveFile = new File(savePath);
        mRotation = rotation;
    }

    @Override
    public String loadInBackground() {

//        Bitmap bmp = BitmapFactory.decodeByteArray(mData, 0, mData.length);

        File dir = mSaveFile.getParentFile();
        if(!dir.exists()){
            dir.mkdirs();
        }

        saveBitmap(mData, mSaveFile);
//        bmp.recycle();

        if(mRotation != 0){
            try {
                ExifInterface exifInterface = new ExifInterface(mSaveFile.getAbsolutePath());

                int exifRotation;

                switch (mRotation){
                    case 90 : exifRotation = ExifInterface.ORIENTATION_ROTATE_90; break;
                    case 180 : exifRotation = ExifInterface.ORIENTATION_ROTATE_90; break;
                    case 270 : exifRotation = ExifInterface.ORIENTATION_ROTATE_90; break;
                    default: exifRotation = ExifInterface.ORIENTATION_NORMAL;
                }

                exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, Integer.toString(exifRotation));
                exifInterface.saveAttributes();

            } catch (IOException e) {
                Log.e("SavePhoto", "Failed to open exif");
            }
        }

        return mSaveFile.getAbsolutePath();
    }

    private String saveBitmap(byte[] data, File fileToSave){
        try{
            FileOutputStream fos = new FileOutputStream(fileToSave.getPath());
            fos.write(data);
            fos.close();
        }catch (IOException ex){
            Log.e("SavePhoto", "Failed to open exif");
        }
        return fileToSave.getAbsolutePath();
    }

    private String saveBitmap(Bitmap bitmap, File fileToSave) {

        FileOutputStream outStream = null;
        BufferedOutputStream bos = null;
        try {
            fileToSave.createNewFile();
            outStream = new FileOutputStream(fileToSave);
            bos = new BufferedOutputStream(outStream, 1024 * 8);
            if (bitmap == null) {
                return null;
            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);

            bos.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                }
            }
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException e) {
                }
            }
        }
        return fileToSave.getAbsolutePath();
    }

}
