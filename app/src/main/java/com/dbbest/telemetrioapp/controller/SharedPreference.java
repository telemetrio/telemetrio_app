package com.dbbest.telemetrioapp.controller;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPreference {
	
	private final static String GENERAL_PREFERENCE = "general_pref";

    private final static String USER_ID_KEY = "user_id";
    private final static String LOGIN_URL_KEY = "login_url";

    private final static String AUTH_KEY = "auth_key";

    private final static String SETTING_PARAM_01_KEY = "param_01";
    private final static String SETTING_PARAM_02_KEY = "param_02";

    private final static String USER_LOGIN_KEY = "user_login";
    private final static String USER_PASSWORD_KEY = "user_password";

    private Context context;
//	private String apiKey;

	private String userID;

	private String authKey;

    private String userLogin;
    private String userPassword;

    private String baseURL = "https://www.telemetrio.com/api/";
    private String loginURL = "signin";

    private int setting_param_01;
    private int setting_param_02;

    double mLatitude; // latitude
    double mLongitude; // longitude

    public SharedPreference(Context context)
	{
		this.context = context.getApplicationContext();
		init();
	}
	
	private void init() {
		SharedPreferences settings = context.getSharedPreferences(GENERAL_PREFERENCE, 0);

        userID = settings.getString(USER_ID_KEY, null);
        authKey = settings.getString(AUTH_KEY, null);
        setting_param_01 = settings.getInt(SETTING_PARAM_01_KEY, 0);
        setting_param_02 = settings.getInt(SETTING_PARAM_02_KEY, 0);
        userLogin = settings.getString(USER_LOGIN_KEY, null);
        userPassword = settings.getString(USER_PASSWORD_KEY, null);
	}

	private SharedPreferences.Editor getEditor()
	{
		SharedPreferences settings = context.getSharedPreferences(GENERAL_PREFERENCE, 0);
		return settings.edit();
	}

// ------------------------------------------------------------------------------------------------------------------

	public void clearAll()
	{
		getEditor().clear().commit();
		init();
	}

// ------------------------------------------------------------------------------------------------------------------

    public boolean setSettingParam_01(int param) {
        this.setting_param_01 = param;
        return getEditor().putInt(SETTING_PARAM_01_KEY, setting_param_01).commit();
    }

    public boolean setSettingParam_02(int param) {
        this.setting_param_02 = param;
        return getEditor().putInt(SETTING_PARAM_02_KEY, setting_param_02).commit();
    }

    public int getSettingParam_01() {
        return setting_param_01;
    }

    public int getSettingParam_02() {
        return setting_param_02;
    }

// ------------------------------------------------------------------------------------------------------------------

    public boolean setUserLogin(String userLogin) {
    this.userLogin = userLogin;
    return getEditor().putString(USER_LOGIN_KEY, userLogin).commit();
}

    public String getUserLogin() {
        return userLogin;
    }

    public boolean setUserPassword(String userPassword) {
        this.userPassword = userPassword;
        return getEditor().putString(USER_PASSWORD_KEY, userPassword).commit();
    }

    public String getUserPassword() {
        return userPassword;
    }

// ------------------------------------------------------------------------------------------------------------------


    public boolean setUserID(String userID) {
        this.userID = userID;
        return getEditor().putString(USER_ID_KEY, userID).commit();
    }

    public String getUserID() {
        return userID;
    }


    public void setLoginURL(String loginURL) {
        this.loginURL = loginURL;
        getEditor().putString(LOGIN_URL_KEY, loginURL).commit();
    }

    public String getLoginURL() {
        return baseURL + loginURL;
    }

    public void setLatLon(double latitude, double longitude) {
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }
    public double getLongitude() {
        return mLongitude;
    }


    public boolean setAuthKey(String authKey) {
        this.authKey = authKey;
        return getEditor().putString(AUTH_KEY, authKey).commit();
    }

    public String getAuthKey() {
        return authKey;
    }
}
