package com.dbbest.telemetrioapp.activity.base;

import android.support.v4.app.Fragment;

public interface FragmentActivityBackStack {
	
	public void pushFragmentToBackStack(Fragment fragment, Boolean withAnimation);
	public void onBackPressed();
	

}
