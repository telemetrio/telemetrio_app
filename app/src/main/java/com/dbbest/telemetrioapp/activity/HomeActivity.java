package com.dbbest.telemetrioapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.util.Logger;

//import com.dbbest.telemetrioapp.provider.ProfileConstants;

public class HomeActivity extends Fragment {

	private static final String TAG = "TelemetrioApp";

    private Context mContext;
    private FragmentActivityBackStack stack;
	private Long requestId;
	private BroadcastReceiver requestReceiver;



    public static HomeActivity newInstance() {
        HomeActivity fragment = new HomeActivity();
        fragment.stack = null;
        return fragment;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home, container,
                false);

        mContext = this.getActivity();

        return view;
    }




	@Override
    public void onResume() {
		super.onResume();

	}

//	private String getNameFromContentProvider() {
//
//		String name = null;
//
//		Cursor cursor = mContext.getContentResolver().query(ProfileConstants.CONTENT_URI, null, null, null, null);
//
//		if (cursor.moveToFirst()) {
//			int index = cursor.getColumnIndexOrThrow(ProfileConstants.NAME);
//			name = cursor.getString(index);
//		}
//
//		cursor.close();
//
//		return name;
//	}

	@Override
    public void onPause() {
		super.onPause();

		// Unregister for broadcast
		if (requestReceiver != null) {
			try {
                mContext.unregisterReceiver(requestReceiver);
			} catch (IllegalArgumentException e) {
				Logger.error(TAG, e.getLocalizedMessage(), e);
			}
		}
	}

	private void showToast(String message) {
			Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
	}

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return true;
//    }


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.logout:
//			AuthorizationManager.getInstance(this).logout();
//			Intent login = new Intent(this, LoginActivity.class);
//			startActivity(login);
//			finish();
			break;
		case R.id.about:
//			Intent about = new Intent(this, AboutActivity.class);
//			startActivity(about);
			break;
		}
		return false;
	}
}
