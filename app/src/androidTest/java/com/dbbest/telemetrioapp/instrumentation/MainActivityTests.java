/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dbbest.telemetrioapp.instrumentation;

import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;

import com.dbbest.telemetrioapp.MainActivity;
import com.dbbest.telemetrioapp.activity.LoginActivity;
import com.dbbest.telemetrioapp.controller.SharedPreference;

public class MainActivityTests extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;
    private Fragment mFragment;
    private SharedPreference sharedPreference;
    private String userId;

    public MainActivityTests() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        sharedPreference = new SharedPreference(mainActivity);
        userId = sharedPreference.getUserID();
        try {
            mFragment = mainActivity.getSupportFragmentManager().getFragments().get(1);
        } catch (IndexOutOfBoundsException ignored){

        }
    }

    public void testLoginAppearIfNotSignedIn() {
        if(!isSignedIn()){
            assertNotNull(mFragment);
            assertTrue(mFragment instanceof LoginActivity);
        }
    }

    private boolean isSignedIn(){
        return userId != null;
    }
}
