package com.dbbest.telemetrioapp.view;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;

public class TimelineView extends RelativeLayout {

    LayoutInflater mInflater;
    private RelativeLayout mMainRelativeLayout;
    private ImageView mTLimage;
    private int mTotalTime;
    private Context mContext;
    int viewWidth = 0;
    int viewHeight = 0;


    public TimelineView(Context context) {
        super(context);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        init();
    }

    public TimelineView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        init();
    }

    public TimelineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        init();
    }

    public void init() {
        View v = mInflater.inflate(R.layout.timeline_view, this, true);

        mMainRelativeLayout = (RelativeLayout) v.findViewById(R.id.timeline_container);
        mTLimage = (ImageView) v.findViewById(R.id.timeline);
    }

    public void setTotalTime(int totalTime) {
        this.mTotalTime = totalTime * 60;
    }

    public int convertDipToPixels(float dips)
    {
        return (int) (dips * mContext.getResources().getDisplayMetrics().density + 0.5f);
    }
    public  int pxToDp( float px)
    {
        return (int) (px / mContext.getResources().getDisplayMetrics().density);
    }

    public int dpToPx( float dp)
    {
        return (int) (dp * mContext.getResources().getDisplayMetrics().density);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        viewWidth = xNew;
        viewHeight = yNew;
                        /*
                        these viewWidth and viewHeight variables
                        are the global int variables
                        that were declared above
                        */
    }

    double trackWidth;
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int size = 0;
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        int widthWithoutPadding = width - getPaddingLeft() - getPaddingRight();
        int heigthWithoutPadding = height - getPaddingTop() - getPaddingBottom();

        double w = (((BitmapDrawable) mTLimage.getBackground()).getBitmap().getWidth());
        trackWidth =  MeasureSpec.getSize(widthMeasureSpec) - 32 * getDeviceDensity() ;//- getPaddingLeft() - getPaddingRight();

//        // set the dimensions
//        if (widthWithoutPadding >  heigthWithoutPadding) {
//            size = heigthWithoutPadding;
//        } else {
//            size = widthWithoutPadding;
//        }
//
//        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size + getPaddingTop() + getPaddingBottom());
    }

    public void addPoint(int time, int type) {
//        double w = (((BitmapDrawable) mTLimage.getBackground()).getBitmap().getWidth());
        double w = trackWidth;
//        w = w*getDeviceDensity();

//        int w = mTLimage.getMeasuredWidth();

        LinearLayout LL = new LinearLayout(getContext());
        LL.setOrientation(LinearLayout.VERTICAL);
        LL.setGravity(Gravity.BOTTOM);

        LayoutParams LLParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LLParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        int leftMargin = getTrackMargin(w - getMeasureWidthError(), (double) mTotalTime, (double) time);
        Log.d("my", " leftMargin " + leftMargin);
        LLParams.setMargins(leftMargin, 0, 0, 0);
        LL.setLayoutParams(LLParams);

        ImageView IV = new ImageView(getContext());
        switch (type) {
            case 1:
                IV.setImageResource(R.drawable.slider_goal);
                break;
            case 2:
                IV.setImageResource(R.drawable.slider_great_play);
                break;
            case 3:
                IV.setImageResource(R.drawable.slider_improve);
                break;
            case 4:
                IV.setImageResource(R.drawable.slider_start);
                break;
            default:
                IV.setImageResource(R.drawable.slider_start);
                break;
        }

        TextView TV = new TextView(getContext());
        TV.setBackgroundColor(Color.WHITE);
        TV.setText(getDurationString(time));

        LinearLayout.LayoutParams paramsTV = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        paramsTV.setMargins(0, 20, 0, 5);
        TV.setLayoutParams(paramsTV);

        TV.setRotation(270);
        TV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);

        LL.addView(TV);
        LL.addView(IV);

        mMainRelativeLayout.addView(LL);

    }

    public static int getTrackMargin(double trackerWidth, double totalTime, double currentTime){
        Log.d("my", "total " + totalTime + " current " + currentTime + " width" + trackerWidth);
        if(currentTime > totalTime) {
            throw new IllegalArgumentException("Current time should be < or == total time");
        }
        double proportion = currentTime / totalTime;

        return (int) ((trackerWidth * proportion) + getMeasureMarginError() - 16*proportion*getDeviceDensity());
    }

    private static int getMeasureMarginError() {
        float density = TelemetryApplication.getAppContext().getResources().getDisplayMetrics().density;
        return (int) (getRealLineMarginPx() * density);
    }

    private static int getMeasureWidthError() {
        float density = TelemetryApplication.getAppContext().getResources().getDisplayMetrics().density;
        return (int) (getRealLineMarginPx() * 2 * density);
    }

    private static int getRealLineMarginPx() {
        return 16;
    }

    private static float getDeviceDensity() {
        return TelemetryApplication.getAppContext().getResources().getDisplayMetrics().density;
    }

    private String getDurationString(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        if (hours < 1) {
            String strHours = "   ";
            return strHours + twoDigitString(minutes) + ":" + twoDigitString(seconds);
        } else {
            String strHours = String.valueOf(hours);
            return  strHours + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);

        }
    }

    private String twoDigitString(int number) {
        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}