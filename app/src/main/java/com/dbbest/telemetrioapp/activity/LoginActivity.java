package com.dbbest.telemetrioapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dbbest.telemetrioapp.MainActivity;
import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.model.User;
import com.dbbest.telemetrioapp.util.ValidateUtils;
import com.dbbest.telemetrioapp.view.custom.FacebookButton;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends Fragment {

    private static final String TAG = LoginActivity.class.getSimpleName();
    public static final String ACTION_LOGIN = "Login";
    public static final String USER_ID = "UserId";
    public static final String USER_NAME = "UserName";
    public static final String EMAIL = "Email";
    public static final String LAST_NAME = "LastName";
    public static final String FIRST_NAME = "FirstName";

    private Context mContext;
    private FragmentActivityBackStack stack;
    private SharedPreference mSharedPreference;

    private boolean isActive;
    private ProgressBar mProgressBar;


    public static LoginActivity newInstance() {
        LoginActivity fragment = new LoginActivity();
        fragment.stack = null;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity();

        mSharedPreference = new SharedPreference(mContext);
        if (getActivity() instanceof FragmentActivityBackStack) {
            stack = (FragmentActivityBackStack) getActivity();
        }

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mLoginReceiver, new IntentFilter(ACTION_LOGIN));
    }

    private BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String email = intent.getStringExtra(EMAIL);
            String lastName = intent.getStringExtra(LAST_NAME);
            String firstName = intent.getStringExtra(FIRST_NAME);
            String userName = intent.getStringExtra(USER_NAME);
            String userId = intent.getStringExtra(USER_ID);

            onLoginClientSocial(userId, userName, firstName, lastName, email);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_screen, container, false);

        Button btn_login = (Button) view.findViewById(R.id.btn_login);
        Button btn_signup = (Button) view.findViewById(R.id.btn_signup);

        final EditText input_mail = (EditText) view.findViewById(R.id.input_mail);
        final EditText input_password = (EditText) view.findViewById(R.id.input_password);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_indicator);

        btn_signup.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                stack.pushFragmentToBackStack(SignupActivity.newInstance(), true);
            }
        });

        btn_login.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
//               String mail = input_mail.getText().toString().trim();
//               String password = input_password.getText().toString().trim();
                String mail = input_mail.getText().toString();
                String password = input_password.getText().toString();

                if (TelemetryApplication.connectionIsAvaible()) {

                    if (ValidateUtils.isValidCredentials(mail, password)) {
                        mProgressBar.setVisibility(View.VISIBLE);

                        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).signin(mail, password, new Callback<User>() {
                            @Override
                            public void success(User user, Response response) {
                                onSuccessLogin(user);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                onFailureLogin(error);
                            }
                        });
                    } else {
                        showToast("Please fill in your user e-mail and password");
                    }

            } else {
                    showToast(getString(R.string.network_error));
                }
            }
        });

        FacebookButton facebookButton = (FacebookButton) view.findViewById(R.id.login_facebook_button);
        facebookButton.initFacebookActions(getActivity(), new FacebookButton.Callback() {
            @Override
            public void onLoggedInFacebook(String userId, String username, String email, String firstName, String lastName) {
                Intent intent = new Intent(LoginActivity.ACTION_LOGIN);
                intent.putExtra(USER_ID, userId);
                intent.putExtra(USER_NAME, username);
                intent.putExtra(EMAIL, email);
                intent.putExtra(FIRST_NAME, firstName);
                intent.putExtra(LAST_NAME, lastName);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        });

        ImageView imageView = (ImageView) view.findViewById(R.id.login_icon_social_google);
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).signIn();
            }
        });

        return view;
    }

    private void onLoginClientSocial(String userId, String username, String firstName, String lastName, String email){
        mProgressBar.setVisibility(View.VISIBLE);

        TelemetryApplication.getRestServiceForAccess(RestAdapter.LogLevel.FULL).loginSocial(userId, firstName, lastName, username, email, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                onSuccessLogin(user);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailureLogin(error);
            }
        });
    }

    private void onSuccessLogin(User user){
        if(isActive) {
            Log.d(TAG, user.toString());
            mProgressBar.setVisibility(View.INVISIBLE);
            mSharedPreference.setUserID(String.valueOf(user.id));
            mSharedPreference.setAuthKey(user.auth.key);

            Intent startHomeActivity = new Intent(mContext, MainTabActivity.class);
            startActivity(startHomeActivity);
            getActivity().finish();
        }
    }

    private void onFailureLogin(RetrofitError error){
        if(isActive) {
            Log.d(TAG, error.toString());
            if (error.toString().equals("404 Not Found")) {
                showToast("Please make sure you fill in a correct email and password");
            } else {
                showToast("Server Unavailable, Please try again later");
            }
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }



    @Override
    public void onPause() {
        super.onPause();

        isActive = false;
    }

	@Override
    public void onResume() {
        super.onResume();

        isActive = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mLoginReceiver);
    }

    private void showToast(String message) {
            Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
    }


}
