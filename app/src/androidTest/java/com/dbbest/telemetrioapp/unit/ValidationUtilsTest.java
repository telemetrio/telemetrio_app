package com.dbbest.telemetrioapp.unit;

import android.test.suitebuilder.annotation.SmallTest;

import com.dbbest.telemetrioapp.util.ValidateUtils;

import junit.framework.TestCase;

public class ValidationUtilsTest extends TestCase{

    @SmallTest
    public void testIsValidCredentials(){
        boolean isValid = ValidateUtils.isValidCredentials("","");
        assertFalse(isValid);

        isValid = ValidateUtils.isValidCredentials("","abc");
        assertFalse(isValid);

        isValid = ValidateUtils.isValidCredentials("abc","");
        assertFalse(isValid);

        isValid = ValidateUtils.isValidCredentials("abcd","123456");
        assertTrue(isValid);
    }
}
