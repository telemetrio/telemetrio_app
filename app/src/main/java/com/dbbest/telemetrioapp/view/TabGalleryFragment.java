package com.dbbest.telemetrioapp.view;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.PhotoVideoActivity;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.imageloader.TelemetrioUIL;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.service.UpdateService;
import com.dbbest.telemetrioapp.service.UploadService;

import java.io.File;

public class TabGalleryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener{

    private static final int URL_EVENT_LOADER = 0;

    private static final int GALLERY_SORT_DATE = 0;
    private static final int GALLERY_SORT_EVENT = 1;

    private int mSort = 0;
    private GridView mGridView;
    private CursorAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private InnerResultReceiver mResultReceiver;
    private RelativeLayout sort_by_date_rl;
    private RelativeLayout sort_by_event_rl;
    private TextView sort_by_date_txt;
    private TextView sort_by_event_txt;
    private ImageView sort_by_date_img;
    private ImageView sort_by_event_img;
    private String mUserId;


    public static TabGalleryFragment newInstance() {
        return new TabGalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreference sharedPreference = new SharedPreference(getActivity());
        mUserId = sharedPreference.getUserID();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_gallery, container, false);
        mGridView = (GridView) view.findViewById(R.id.gridViewPhotos);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mRefreshLayout.setOnRefreshListener(this);



//        UpdateService.startActionFetchNewPhotos(getActivity(), mUserId, mResultReceiver);



        sort_by_date_rl = (RelativeLayout)view.findViewById(R.id.sort_by_date_rl);
        sort_by_event_rl = (RelativeLayout)view.findViewById(R.id.sort_by_event_rl);
        sort_by_date_txt = (TextView)view.findViewById(R.id.sort_by_date_txt);
        sort_by_event_txt = (TextView)view.findViewById(R.id.sort_by_event_txt);
        sort_by_date_img = (ImageView)view.findViewById(R.id.sort_by_date_img);
        sort_by_event_img = (ImageView)view.findViewById(R.id.sort_by_event_img);

//        //FIXME: remove this button from layout, it's only for testing auto-update UI
//        view.findViewById(R.id.button_clean).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getContentResolver().delete(TelemetrioContract.Photos.CONTENT_URI, null, null);
//            }
//        });


        sort_by_date_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSort(GALLERY_SORT_DATE);
            }
        });
        sort_by_event_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSort(GALLERY_SORT_EVENT);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new SimpleGalleryAdapter(getActivity(), null);
        mGridView.setAdapter(mAdapter);
        mResultReceiver = new InnerResultReceiver(new Handler());

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(URL_EVENT_LOADER, null, this);

        onRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        mResultReceiver.setReceiverListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mResultReceiver.setReceiverListener(null);
    }

    // These are the Contacts rows that we will retrieve.
    static final String[] EVENTS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Photos._ID,
            TelemetrioContract.Photos.PHOTO_ID,
            TelemetrioContract.Photos.PHOTO_IMAGE,
            TelemetrioContract.Photos.PHOTO_FILE,
            TelemetrioContract.Photos.PHOTO_CLUB_ID,
            TelemetrioContract.Photos.PHOTO_TEAM_ID,
            TelemetrioContract.Photos.PHOTO_USER_ID,
            TelemetrioContract.Photos.PHOTO_THUMBNAIL,
            TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS
    };


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.

        if (id == URL_EVENT_LOADER) {
            Uri baseUri = TelemetrioContract.Photos.CONTENT_URI;

            if(GALLERY_SORT_DATE ==  mSort) {
                return new CursorLoader(getActivity(), baseUri,
                        EVENTS_SUMMARY_PROJECTION, null, null,
                        TelemetrioContract.Photos.DEFAULT_SORT);
            } else {
                return new CursorLoader(getActivity(), baseUri,
                        EVENTS_SUMMARY_PROJECTION, null, null,
                        TelemetrioContract.Photos.EVENT_SORT);

            }
        }
        else{
            return null;
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        mAdapter.swapCursor(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }

    @Override
    public void onRefresh() {
        UpdateService.startActionFetchNewPhotos(getActivity(), mUserId, mResultReceiver);
        UploadService.startActionUploadMedia(getActivity());
    }

    private static class InnerResultReceiver extends ResultReceiver {
        private TabGalleryFragment fragment;

        public InnerResultReceiver(Handler handler) {
            super(handler);
        }

        public void setReceiverListener(TabGalleryFragment fragment) {
            this.fragment = fragment;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (fragment != null) {
                fragment.mRefreshLayout.setRefreshing(false);
            } else {
                Log.w("TAG", "Can't find fragment");
            }
        }
    }

    private static class SimpleGalleryAdapter extends CursorAdapter {

        private final LayoutInflater inflater;
        private Integer mImageWidth;
        private Integer mImageHeight;
        private Context mContext;

        private static final class GalleryItemHolder{
            ImageView photoImageView;
            ImageView photoBackLeft;
        }

        public SimpleGalleryAdapter(Context context, Cursor c)
        {
            super(context, c, true);
            mContext = context;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = inflater.inflate(R.layout.grid_item_gallery, parent, false);
            GalleryItemHolder holder = new GalleryItemHolder();
            holder.photoImageView = (ImageView) view.findViewById(R.id.photo);
            holder.photoBackLeft = (ImageView) view.findViewById(R.id.photo_back_left);

//            Bitmap b = ((BitmapDrawable)holder.photoImageView.getBackground()).getBitmap();
//            mImageWidth = b.getWidth();
//            mImageHeight = b.getHeight();


            WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            Display disttt = wm.getDefaultDisplay();
            Point size = new Point();
            disttt.getSize(size);
            int width = size.x-12;
            int height = size.y;

//            int rowCount = 3;

//            if((width/rowCount) > mImageWidth) {
//                mImageWidth = (width/rowCount);
//            }


            view.setTag(holder);
            return view;
        }

        @Override
        public void bindView(View v, final Context context, final Cursor c) {
            Uri uri;
            int status = c.getInt(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS));
            if (status == 0){
                uri = Uri.fromFile(new File(c.getString(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_THUMBNAIL))));
            }
            else{
                uri = Uri.parse(c.getString(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_THUMBNAIL)));
            }
            GalleryItemHolder holder = (GalleryItemHolder) v.getTag();

            final int image = c.getInt(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_IMAGE));
            final String id = c.getString(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_ID));
            final String url = c.getString(c.getColumnIndex(TelemetrioContract.Photos.PHOTO_FILE));

            if (0 == image) {
                holder.photoImageView.setImageResource(R.drawable.video_icon);
                if (1 == status) {
                    TelemetrioUIL.getInstance().displayImage(uri.toString(), holder.photoImageView, TelemetrioUIL.getDisplayOptions());
                }
            } else {
                TelemetrioUIL.getInstance().displayImage(uri.toString(), holder.photoImageView, TelemetrioUIL.getDisplayOptions());
            }


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(v.getContext(), id, Toast.LENGTH_SHORT).show();
                    if(image == 0) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        Uri data = Uri.parse(url);
                        browserIntent.setDataAndType(data, "video/*");
                        mContext.startActivity(browserIntent);
                    } else{
                        PhotoVideoActivity.start((android.app.Activity) mContext, id);
                    }
                }
            });

        }
    }


    private void setSort(int sort){

        switch (sort) {
            case GALLERY_SORT_DATE:

                mSort = GALLERY_SORT_DATE;

                sort_by_date_img.setImageDrawable(getResources().getDrawable(R.drawable.tab_select_sel));
                sort_by_event_img.setImageDrawable(getResources().getDrawable(R.drawable.tab_select_norm));

                sort_by_date_txt.setTextColor(Color.parseColor("#52555C"));
                sort_by_event_txt.setTextColor(Color.parseColor("#8990A0"));


                break;
            case GALLERY_SORT_EVENT:

                mSort = GALLERY_SORT_EVENT;

                sort_by_event_img.setImageDrawable(getResources().getDrawable(R.drawable.tab_select_sel));
                sort_by_date_img.setImageDrawable(getResources().getDrawable(R.drawable.tab_select_norm));

                sort_by_event_txt.setTextColor(Color.parseColor("#52555C"));
                sort_by_date_txt.setTextColor(Color.parseColor("#8990A0"));


                break;

        }
        getLoaderManager().restartLoader(URL_EVENT_LOADER, null, this);
    }


}
