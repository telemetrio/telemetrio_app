package com.dbbest.telemetrioapp.unit;


import android.test.suitebuilder.annotation.SmallTest;

import com.dbbest.telemetrioapp.view.TimelineView;

import junit.framework.TestCase;

import static junit.framework.Assert.assertEquals;

public class TimelineViewTest extends TestCase{

    @SmallTest
    public void testTrackMargin(){
        double viewWidth = 10.0;
        double totalTime = 2;
        double currentTime = 1;

        int expected = 21;
        int actual = TimelineView.getTrackMargin(viewWidth, totalTime, currentTime);

        assertEquals(expected, actual);
    }

    @SmallTest
    public void testTrackMarginException(){
        double viewWidth = 10.0;
        double totalTime = 2;
        double currentTime = 3;

        try {
            TimelineView.getTrackMargin(viewWidth, totalTime, currentTime);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }

    }
}
