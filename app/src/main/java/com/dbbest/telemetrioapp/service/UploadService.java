package com.dbbest.telemetrioapp.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.google.common.io.Files;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class UploadService extends IntentService {

    private static final String ACTION_UPLOAD_MEDIA = "com.dbbest.telemetrioapp.service.action.uploadmedia";
    private static final String TAG = "UploadService";

    /**
     * Starts this service to perform action ACTION_UPLOAD_MEDIA. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionUploadMedia(Context context) {
        Intent intent = new Intent(context, UploadService.class);
        intent.setAction(ACTION_UPLOAD_MEDIA);
        context.startService(intent);
    }

    public UploadService() {
        super("UploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPLOAD_MEDIA.equals(action)) {
                handleUploadRequest();
            }
        }
    }

    // These are the Photos rows that we will retrieve.
    static final String[] PHOTOS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Photos._ID,
            TelemetrioContract.Photos.PHOTO_ID,
            TelemetrioContract.Photos.PHOTO_LOCAL_ID,
            TelemetrioContract.Photos.PHOTO_CLUB_ID,
            TelemetrioContract.Photos.PHOTO_TEAM_ID,
            TelemetrioContract.Photos.PHOTO_USER_ID,
            TelemetrioContract.Photos.PHOTO_FILE,
            TelemetrioContract.Photos.PHOTO_IMAGE,
            TelemetrioContract.Photos.PHOTO_DESCRIPTION,
            TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS
    };

    static final String WHERE =  TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS + "=?";
    static final String[] ARGS =  {"0"};

    /**
     * Handle request for uploading media files
     */
    private void handleUploadRequest() {
        SharedPreference sharedPreference = new SharedPreference(this);

        final Uri baseUri = TelemetrioContract.Photos.CONTENT_URI;
        Cursor cursor = getContentResolver().query(baseUri, PHOTOS_SUMMARY_PROJECTION, WHERE, ARGS, TelemetrioContract.Photos.DEFAULT_SORT);

        int idIndex = cursor.getColumnIndex(TelemetrioContract.Photos._ID);
        int photoIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_ID);
        int photoLocalIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_LOCAL_ID);
        int fileIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_FILE);
        int clubIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_CLUB_ID);
        int teamIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_TEAM_ID);
        int userIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_USER_ID);
        int imageIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_IMAGE);
        int descriptionIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_DESCRIPTION);



        List<UploadEntity> photosToUpload = new ArrayList<>();

        while (cursor.moveToNext()) {
            UploadEntity uploadEntity = new UploadEntity();
            uploadEntity.id = cursor.getString(idIndex);
            uploadEntity.photoID = cursor.getString(photoIdIndex);
            uploadEntity.photoLocalID = cursor.getString(photoLocalIdIndex);
            uploadEntity.fileUrl = cursor.getString(fileIndex);
            uploadEntity.userId = sharedPreference.getUserID();
            uploadEntity.clubId = cursor.getString(clubIdIndex);
            uploadEntity.teamId = cursor.getString(teamIdIndex);
            uploadEntity.image = cursor.getString(imageIndex);
            uploadEntity.description = cursor.getString(descriptionIndex);
            photosToUpload.add(uploadEntity);
        }
        cursor.close();

        Log.d(TAG, "photosToUpload " + photosToUpload.toString());

        for(final UploadEntity uploadEntity : photosToUpload){
            if (null != uploadEntity) {
                Log.d(TAG, "Uploading start for key with file path " + uploadEntity.fileUrl);
            }

            TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).upload(uploadEntity.getTypedFile(),
                    new TypedString(uploadEntity.userId),
                    new TypedString(uploadEntity.photoLocalID),
                    new TypedString(uploadEntity.image),
                    new TypedString(uploadEntity.description),

                    new Callback<UploadResponseEntity>() {
                @Override
                public void success(UploadResponseEntity uploadResponseEntity, Response response) {
                    if (null != uploadResponseEntity) {
                        Log.d(TAG, "success: " + uploadResponseEntity.toString());
                    ContentValues values = new ContentValues();
                    values.put(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS, 1);
                    values.put(TelemetrioContract.Photos.PHOTO_ID, uploadResponseEntity.photo_id);
                    values.put(TelemetrioContract.Photos.PHOTO_THUMBNAIL, uploadResponseEntity.thumbnail);
                    String where = TelemetrioContract.Photos.PHOTO_LOCAL_ID + "=?";
                    String[] args = {uploadEntity.photoLocalID};
                    getContentResolver().update(baseUri, values, where, args);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    try{
                        Log.d(TAG, "failure: " + new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                    } catch (Exception ignored) {
                        Log.d(TAG, "failure: " + "cannot parse response");
                    }
                }
            });


        }

    }

    class UploadEntity{
        public String id;
        public String photoID;
        public String photoLocalID;
        public String fileUrl;
        public String userId;
        public String clubId;
        public String teamId;
        public String image;
        public String description;

        public TypedFile getTypedFile(){
            File file = new File(fileUrl);
            String fileExtension = Files.getFileExtension(file.getName());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            Log.d("UploadEntity", "get upload entity with" + " fileName: " + file.getName() + ", extension: " + fileExtension + ", mimeType:" + mimeType);
            return new TypedFile(mimeType, file);
        }

        @Override
        public String toString() {
            return "UploadEntity{" +
                    "id='" + id + '\'' +
                    ", photoLocalID='" + photoLocalID + '\'' +
                    ", fileUrl='" + fileUrl + '\'' +
                    ", userId='" + userId + '\'' +
                    ", clubId='" + clubId + '\'' +
                    ", teamId='" + teamId + '\'' +
                    '}';
        }
    }

    public class UploadResponseEntity{
        public String photo_id;
        public String local_id;
        public String file;
        public String thumbnail;

        @Override
        public String toString() {
            return "UploadResponseEntity{" +
                    "photo_id='" + photo_id + '\'' +
                    ", local_id='" + local_id + '\'' +
                    ", file='" + file + '\'' +
                    ", thumbnail='" + thumbnail + '\'' +
                    '}';
        }
    }

}
