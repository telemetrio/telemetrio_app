package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;

public class GetConfiguration {

    @SerializedName("push_notifications")
    public PushNotifications pushNotifications;

    @SerializedName("email_weekly")
    public EmailWeekly emailWeekly;

    public class PushNotifications {
        @SerializedName("setting")
        public int setting;
        @SerializedName("id")
        public int id;
        @SerializedName("state")
        public int state;

        @Override
        public String toString() {
            return "PushNotifications{" +
                    "setting=" + setting +
                    ", id=" + id +
                    ", state=" + state +
                    '}';
        }
    }

    public class EmailWeekly {
        @SerializedName("setting")
        public int setting;
        @SerializedName("id")
        public int id;
        @SerializedName("state")
        public int state;

        @Override
        public String toString() {
            return "EmailWeekly{" +
                    "setting=" + setting +
                    ", id=" + id +
                    ", state=" + state +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GetConfiguration{" +
                "pushNotifications=" + pushNotifications +
                ", emailWeekly=" + emailWeekly +
                '}';
    }
}
