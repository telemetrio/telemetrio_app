package com.dbbest.telemetrioapp.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.dbbest.telemetrioapp.R;


public class AlertHelper {

    public static void showLogoutAlert(Context context, DialogInterface.OnClickListener onClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.alert_logout)
                .setCancelable(true)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("OK", onClickListener);

        AlertDialog alert = builder.create();
        alert.show();
    }
}
