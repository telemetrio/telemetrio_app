package com.dbbest.telemetrioapp.view.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.model.Event;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.util.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ScheduleListAdapter  extends CursorAdapter {
	
	private Context context;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_NORMAL = 0;


	public ScheduleListAdapter (Context context, Cursor c) {
        super(context, c);

		this.context = context;
	}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.row_schedule_adapter, parent, false);
        return retView;
    }

//    @Override
//    public View newView(Context context, Cursor cursor, ViewGroup parent) {
//        LayoutInflater inflater = LayoutInflater.from(context);
//
//        View v = inflater.inflate(R.layout.row_schedule_adapter, parent, false);
//        bindView(v, context, cursor);
//        return v;
//    }

//    @Override
//    public Object getItem(int position) {
//
////        Object item = (Event) super.getItem(position);
//
//
//        return super.getItem(position);
////        return item;
//    }

    public int getItemIsToday(int position) {
        int ret = 0;
//        Object item = super.getItem(position);
        Cursor cursor = getCursor();
        if(cursor.moveToPosition(position)) {
//            Event item = new Event();

            String sEventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String sEventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(sEventTimeBegin));

            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(sEventTimeEnd));

            Date eventTimeBegin = calEventTimeBegin.getTime();
            Date eventTimeEnd = calEventTimeEnd.getTime();

            String dateFormat = "yyyy MM dd";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

//        String timeFormat = "HH:mm";
//        SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

            Calendar currentTime = Calendar.getInstance();
            Date mCurrentTime = currentTime.getTime();

//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            // Create a calendar object with today date. Calendar is in java.util pakage.
            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DATE, -1);

            String dateFormatParse = "yyyy MM dd 00:01:01";
            SimpleDateFormat sdfYesterday = new SimpleDateFormat(dateFormatParse);

            Date mYesterday = calendar.getTime();
            String strYesterday = sdfYesterday.format(mYesterday).toString();


            calendar.add(Calendar.DATE, 1);

            mYesterday = calendar.getTime();
            String strTomorrow = sdfYesterday.format(mYesterday).toString();



            if (sdf.format(eventTimeBegin).equals(sdf.format(mCurrentTime))) {
                ret = 1;
            }
            if (eventTimeBegin.before(mCurrentTime) && eventTimeEnd.after(mCurrentTime)) {
                ret = 2;
            }
            try{
                Date parsed = sdf.parse(strYesterday);
                if (eventTimeEnd.before(parsed)) {
                    ret = 3;
                }
            } catch(Exception e){
                System.out.println(e.getMessage());
            }

            try{
                Date parsed = sdf.parse(strTomorrow);
                if (eventTimeBegin.after(parsed)) {
                    ret = 4;
                }
            } catch(Exception e){
                System.out.println(e.getMessage());
            }

        }
        return ret;
    }

    @Override
    public int getItemViewType(int position) {
        int ret =  TYPE_NORMAL;
        if(0 == position) {
            ret =  TYPE_HEADER;
        }
        else {
            Cursor cursor = getCursor();
            if (cursor.moveToPosition(position)) {

                int prev_item_today = getItemIsToday(position - 1);
                int item_today = getItemIsToday(position);


                if (item_today == prev_item_today) {
                    ret = TYPE_NORMAL;
                } else {
                    ret = TYPE_HEADER;
                }

                if ((1 == item_today  || 2 == item_today )  && (1 == prev_item_today || 2 == prev_item_today)) {
                    ret = TYPE_NORMAL;
                }
            }
        }

        return ret;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView row_event_type = (TextView)view.findViewById(R.id.row_event_type);
        TextView row_event_teams = (TextView)view.findViewById(R.id.row_event_teams);
        TextView row_event_name_event = (TextView)view.findViewById(R.id.row_event_name_event);

        TextView row_event_name_number = (TextView)view.findViewById(R.id.row_event_name_number);
        TextView row_event_day_type = (TextView)view.findViewById(R.id.row_event_day_type);


        TextView row_event_txt_location = (TextView)view.findViewById(R.id.row_event_txt_location);
        TextView row_event_txt_date = (TextView)view.findViewById(R.id.row_event_txt_date);
        TextView row_event_txt_time = (TextView)view.findViewById(R.id.row_event_txt_time);

        ImageView row_event_icon_state = (ImageView)view.findViewById(R.id.row_event_icon_state);
        TextView row_event_txt_state = (TextView)view.findViewById(R.id.row_event_txt_state);


        String[] names = cursor.getColumnNames();

        Logger.debug("NAMES", names.toString());


        String dateFormat = "dd, MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        String timeFormat = "HH:mm";
        SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

        int row_today = getItemIsToday(cursor.getPosition());
        int row_type = getItemViewType(cursor.getPosition());


        if(row_type == TYPE_HEADER) {
            row_event_day_type.setVisibility(View.VISIBLE);


            if(1 == row_today || 2 == row_today) {  //current || today
                row_event_day_type.setText(R.string.row_event_day_type_01);
            } else if(4 == row_today) {  // coming
                row_event_day_type.setText(R.string.row_event_day_type_02);
            } else {
                row_event_day_type.setText(R.string.row_event_day_type_03);
            }
        } else {
            row_event_day_type.setVisibility(View.GONE);
        }

            row_event_icon_state.setVisibility(View.VISIBLE);
            row_event_txt_state.setVisibility(View.VISIBLE);


            if(2 == row_today) {  //current
                row_event_icon_state.setImageResource(R.drawable.row_event_status_online);
                row_event_txt_state.setText(R.string.row_event_txt_state_1);
            } else if((1 == row_today) || (4 == row_today)) {   // today | new
                row_event_icon_state.setImageResource(R.drawable.row_event_status_offline);
                row_event_txt_state.setText(R.string.row_event_txt_state_2);
            } else {
                row_event_icon_state.setVisibility(View.GONE);
                row_event_txt_state.setVisibility(View.GONE);
            }



        row_event_name_number.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_NAME)) + " " +
                        cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_ID))
        );

        row_event_type.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_LEAGUE_NAME)));

        row_event_teams.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME)) + " VS " +
                                cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME))
        );

        row_event_name_event.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_NAME)));

        String eventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
        String eventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

        Calendar calEventTimeBegin = Calendar.getInstance();
        calEventTimeBegin.setTimeInMillis(Long.valueOf(eventTimeBegin));
        Date mEventTimeBegin = calEventTimeBegin.getTime();

        String strFormattedDateBegin = sdf.format(mEventTimeBegin );
        row_event_txt_date.setText(strFormattedDateBegin);

        Calendar calEventTimeEnd = Calendar.getInstance();
        calEventTimeEnd.setTimeInMillis(Long.valueOf(eventTimeEnd));
        Date mEventTimeEnd = calEventTimeEnd.getTime();
        String strFormattedTimeBegin = stf.format(mEventTimeBegin );
        String strFormattedTimeEnd = stf.format(mEventTimeEnd );

        row_event_txt_time.setText( strFormattedTimeBegin + " - " + strFormattedTimeEnd);


    }




}
