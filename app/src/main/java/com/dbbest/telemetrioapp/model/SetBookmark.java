package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;


public class SetBookmark {

    @SerializedName("bookmark_id")
    public long bookmark_id;            //Bookmark identifier
    @SerializedName("event_id")
    public long event_id;            //Event identifier

    @SerializedName("bookmark_action")
    public long bookmark_action;
    @SerializedName("bookmark_time")
    public long bookmark_time;
    @SerializedName("bookmark_location_lat")
    public double bookmark_location_lat;
    @SerializedName("bookmark_location_long")
    public double bookmark_location_long;


    @Override
    public String toString() {
        return "SetBookmark{" +
                "bookmark_id=" + bookmark_id +
                ", event_id=" + event_id +
                ", bookmark_action=" + bookmark_action +
                ", bookmark_time=" + bookmark_time +
                ", bookmark_location_lat=" + bookmark_location_lat +
                ", bookmark_location_long=" + bookmark_location_long +
                '}';
    }
}
