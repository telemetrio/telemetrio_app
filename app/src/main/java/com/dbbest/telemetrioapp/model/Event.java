package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by Andrew Druk on 11/27/14.
 */
public class Event {

    public static final Integer FIRST_TEAM_KEY = 1;
    public static final Integer SECOND_TEAM_KEY = 2;

    @SerializedName("idGame")
    public long idGame;
    @SerializedName("idLeague")
    public long leagueId;
    @SerializedName("NameLeague")
    public String leagueName;
    @SerializedName("time_begin")
    public Date startTime;
    @SerializedName("time_end")
    public Date endTime;
    @SerializedName("idField")
    public int fieldId;
    @SerializedName("FieldName")
    public String fieldName;
    @SerializedName("Country")
    public String country;
    public HashMap<Integer, EventTeam> teams;

    /*WARNING! Unused fields (can be null)*/

    public String streaming_address;
    public String embedded_channel;
    public int age;
    public int idGender;
    public int season_year;
    public int idSeason;
    public int state;
    public int mail_sent;
    public int mail_sent_ready;
    public int on_demand;
    public int idTeamGame;
    public int idTeam;
    public String file;
    public int row_type;
    public int row_today = 0;

    public EventTeam getTeam(int number){
        return teams.get(number);
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + idGame +
                ", leagueId=" + leagueId +
                ", leagueName='" + leagueName + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", fieldId=" + fieldId +
                ", fieldName='" + fieldName + '\'' +
                ", teams=" + teams +
                ", country='" + country + '\'' +
                ", row_type='" + row_type + '\'' +
                ", row_today='" + row_today + '\'' +
                '}';
    }

    public class EventTeam{
        @SerializedName("team_id")
        public long teamId;
        @SerializedName("team_name")
        public String teamName;
        public String logo;
        public int goals;

        @Override
        public String toString() {
            return "EventTeam{" +
                    "team_id=" + teamId +
                    ", team_name='" + teamName + '\'' +
                    ", logo='" + logo + '\'' +
                    ", goals=" + goals +
                    '}';
        }
    }


}
