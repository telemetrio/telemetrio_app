package com.dbbest.telemetrioapp.model;
import com.google.gson.annotations.SerializedName;


public class RegisterDevice {

    @SerializedName("email")
    public long email;    
    @SerializedName("gcm_regid")
    public String gcm_regid;

    @Override
    public String toString() {
        return "RegisterDevice{" +
                "email=" + email +
                ", gcm_regid=" + gcm_regid +
                '}';
    }
}
