package com.dbbest.telemetrioapp.model;

/**
 * Created by Andrew Druk on 11/26/14.
 */
public class UserSignup {

    public long user_id;
    public String fullname;
    public String last_name;
    public String user_login;
    public String user_nicename;
    public String user_email;
    public String activation_key;
    public String address;
    public String error;

    @Override
    public String toString() {
        return "UserSignup{" +
                "user_id=" + user_id +
                ", fullname='" + fullname + '\'' +
                ", last_name='" + last_name + '\'' +
                ", user_login='" + user_login + '\'' +
                ", user_nicename='" + user_nicename + '\'' +
                ", user_email='" + user_email + '\'' +
                ", activation_key=" + activation_key +
                ", error=" + error +
                ", address='" + address +
                '}';
    }
}
