package com.dbbest.telemetrioapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dbbest.telemetrioapp.MainActivity;
import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.model.SetBookmark;
import com.dbbest.telemetrioapp.model.SetConfiguration;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.util.AlertHelper;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SettingsActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>{

	private static final String TAG = "SettingsActivity";
    public static final int PAGE_FAQ = 0;
    public static final int PAGE_TOS = 1;
    private ToggleButton ParamToggleButton_03;
    private ToggleButton ParamToggleButton_04;
    private RelativeLayout mRow_01;
    private RelativeLayout mRow_02;
    private RelativeLayout mRow_03;
    private RelativeLayout mRow_04;
    private RelativeLayout mRow_05;
    private RelativeLayout mRow_06;
    private RelativeLayout mRow_07;
    private RelativeLayout mRow_08;
    private RelativeLayout mRow_09;
    private RelativeLayout mRow_Logout;
    private TextView textView_03_state;
    private TextView textView_04_state;
    public SharedPreference sharedPreference;
    private int setting_weekly_mail;
    private int setting_push_notification;
    private String mUserId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        sharedPreference = new SharedPreference(getBaseContext());

        mUserId = sharedPreference.getUserID();

        setting_weekly_mail = sharedPreference.getSettingParam_01();
        setting_push_notification = sharedPreference.getSettingParam_02();

        setContentView(R.layout.settings_activity);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText(getString(R.string.settings).toUpperCase());


        View backButton = findViewById(R.id.toolbar_left_button);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsActivity.this.onBackPressed();
            }
        });


        ParamToggleButton_03 = (ToggleButton) findViewById(R.id.ParamToggleButton_03);
        ParamToggleButton_04 = (ToggleButton) findViewById(R.id.ParamToggleButton_04);

        textView_03_state = (TextView) findViewById(R.id.textView_03_state);
        textView_04_state = (TextView) findViewById(R.id.textView_04_state);

        setParamView(setting_weekly_mail, ParamToggleButton_03, textView_03_state);
        setParamView(setting_push_notification, ParamToggleButton_04, textView_04_state);

        ParamToggleButton_03
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            textView_03_state.setText(getText(R.string.state_on));
                            setting_weekly_mail = 1;
                        } else {
                            textView_03_state.setText(getText(R.string.state_off));
                            setting_weekly_mail = 0;
                            showToast(getString(R.string.warning_mail));
                        }
                        sharedPreference.setSettingParam_01(setting_weekly_mail);

                        TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).
                                setConfiguration(mUserId, "2", String.valueOf(setting_weekly_mail), new Callback<SetConfiguration>() {
                                    @Override
                                    public void success(SetConfiguration serverResp, Response response) {
                                        Log.d("TAG", serverResp.toString());
                                    }


                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.d("TAG", error.toString());
                                        showToast("Server ERROR");
                                    }
                                });

                    }
                });

        ParamToggleButton_04
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            textView_04_state.setText(getText(R.string.state_on));
                            setting_push_notification = 1;
                        } else {
                            textView_04_state.setText(getText(R.string.state_off));
                            setting_push_notification = 0;
                            showToast(getString(R.string.warning_push));
                        }
                        sharedPreference.setSettingParam_02(setting_push_notification);

                        TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).
                                setConfiguration(mUserId, "1", String.valueOf(setting_push_notification), new Callback<SetConfiguration>() {
                                    @Override
                                    public void success(SetConfiguration serverResp, Response response) {
                                        Log.d("TAG", serverResp.toString());
                                    }


                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.d("TAG", error.toString());
                                        showToast("Server ERROR");
                                    }
                                });
                    }
                });


        mRow_01 = (RelativeLayout) findViewById(R.id.row_01);
        mRow_02 = (RelativeLayout) findViewById(R.id.row_02);
        mRow_03 = (RelativeLayout) findViewById(R.id.row_03);
        mRow_04 = (RelativeLayout) findViewById(R.id.row_04);
        mRow_05 = (RelativeLayout) findViewById(R.id.row_05);
        mRow_06 = (RelativeLayout) findViewById(R.id.row_06);
        mRow_07 = (RelativeLayout) findViewById(R.id.row_07);
        mRow_08 = (RelativeLayout) findViewById(R.id.row_08);
        mRow_09 = (RelativeLayout) findViewById(R.id.row_09);
        mRow_Logout = (RelativeLayout) findViewById(R.id.row_Logout);



        mRow_01.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showInputDialog(1);
            }
        });

        mRow_02.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showInputDialog(2);
            }
        });


        mRow_05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent faqActivity = new Intent(SettingsActivity.this, WebActivity.class);
                faqActivity.putExtra("page", PAGE_FAQ);

                startActivity(faqActivity);
            }
        });

        mRow_08.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent faqActivity = new Intent(SettingsActivity.this, WebActivity.class);
                faqActivity.putExtra("page", PAGE_TOS);
                startActivity(faqActivity);
            }
        });
        mRow_09.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent faqActivity = new Intent(SettingsActivity.this, WebActivity.class);
                faqActivity.putExtra("page", PAGE_TOS);
                startActivity(faqActivity);
            }
        });

        mRow_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertHelper.showLogoutAlert(SettingsActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPreference.setUserID(null);
                        MainActivity.startInNewTask(SettingsActivity.this);
                    }
                });
            }
        });
    }

    protected void showInputDialog(int data) {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(SettingsActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SettingsActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView
                .findViewById(R.id.edittext);
        TextView mListName = (TextView) promptView.findViewById(R.id.list_name);

        final int lData = data;

        if(1 == data) {
            mListName.setText(getText(R.string.enter_name));
            if(null != sharedPreference.getUserLogin()) {
                editText.setText(sharedPreference.getUserLogin());
            }
        } else {
            mListName.setText(getText(R.string.enter_password));
            if(null != sharedPreference.getUserPassword()) {
                editText.setText(sharedPreference.getUserPassword());
            }
        }
        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        resultText.setText("Hello, " + editText.getText());
//                        showToast("OK");
                        String strData = editText.getText().toString();
                        if (null != strData && !strData.equals("")) {
                            if (1 == lData) {
                                sharedPreference.setUserLogin(strData);
                            } else {
                                sharedPreference.setUserPassword(strData);
                            }
                        }
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

    }


    private void setParamView(int param, ToggleButton paramToggleButton, TextView textView) {

        if (1 == param) {
            textView.setText(getText(R.string.state_on));
            paramToggleButton.setChecked(true);

        } else {
            textView.setText(getText(R.string.state_off));
            paramToggleButton.setChecked(false);
        }

    }


    @Override
    public void onResume() {
		super.onResume();

	}



	@Override
    public void onPause() {
		super.onPause();

	}

	private void showToast(String message) {
			Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
	}

    // These are the Events rows that we will retrieve.
    static final String[] SETTING_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Setting.SETTING_USER_ID,
            TelemetrioContract.Setting.SETTING_SETTING,
            TelemetrioContract.Setting.SETTING_STATE,

    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

            Uri baseUri = TelemetrioContract.Setting.CONTENT_URI;

            return new CursorLoader(this, baseUri,
                    SETTING_SUMMARY_PROJECTION, null, null,
                    TelemetrioContract.Events.DEFAULT_SORT);
//            }
//        } else if (id == SET_BOOKMARK_LOADER) {

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
