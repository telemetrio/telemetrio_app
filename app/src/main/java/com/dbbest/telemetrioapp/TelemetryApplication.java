package com.dbbest.telemetrioapp;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.dbbest.telemetrioapp.imageloader.TelemetrioUIL;
import com.dbbest.telemetrioapp.telemetriorest.TelemetrioService;
import com.dbbest.telemetrioapp.util.Logger;

import java.util.Random;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.Converter;


public class TelemetryApplication extends Application {

    String SENDER_ID = "296049960769";


    private static Context mAppContext;



    @Override
	public void onCreate() {
		super.onCreate();

        mAppContext = getApplicationContext();

        Logger.setAppTag(getString(R.string.app_log_tag));
        Logger.setLevel(Logger.DEBUG);

        TelemetrioUIL.init(mAppContext);



    }





    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }




    //    static final String AB = "0123456789ABCDEF";
    static final String AB = "0123456789";
    static Random rnd = new Random();

    public static String randomString(int len)
    {
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }



    public static TelemetrioService getRestServiceForAccess(RestAdapter.LogLevel logLevel) {
        RestAdapter.Builder builder = getDefaultRestAdapterBuilder();
        return builder.setLogLevel(logLevel).build().create(TelemetrioService.class);
    }

    public static TelemetrioService getRestService(final String authKey, final String userIDKey, RestAdapter.LogLevel logLevel) {
        RestAdapter.Builder builder = getDefaultRestAdapterBuilder();
        return builder.setRequestInterceptor(getTelemetrioHeaders(authKey, userIDKey)).setLogLevel(logLevel).
                build().create(TelemetrioService.class);
    }

    public static TelemetrioService getRestServiceWithConverter(final String authKey, final String userIDKey, RestAdapter.LogLevel logLevel, Converter converter) {
        RestAdapter.Builder builder = getDefaultRestAdapterBuilder();
        return builder.setRequestInterceptor(getTelemetrioHeaders(authKey, userIDKey)).
                setConverter(converter).setLogLevel(logLevel).
                build().create(TelemetrioService.class);
    }

    private static RestAdapter.Builder getDefaultRestAdapterBuilder(){
        return new RestAdapter.Builder()
                .setEndpoint(TelemetrioService.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL);
    }

    private static RequestInterceptor getTelemetrioHeaders(final String authKey, final String userIDKey){
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(TelemetrioService.HEADER_API_KEY, authKey);
                request.addHeader(TelemetrioService.HEADER_USER_ID_KEY, userIDKey);
            }
        };
    }

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Log.d("LOWMEMORY", "Huston, we have a problem!");
	}


    public  static Boolean connectionIsAvaible() {
        Context context = getAppContext();
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = cm.getActiveNetworkInfo();
        if (i == null){
//            DataSourceController.setNetworkConnected(false);
            return false;
        }
        if (!i.isConnected()){
//            DataSourceController.setNetworkConnected(false);
            return false;
        }
        if (!i.isAvailable()){
//            DataSourceController.setNetworkConnected(false);
            return false;
        }
//        DataSourceController.setNetworkConnected(true);
        return true;
    }

    /**
     * Returns the application's context. Useful for classes that need a Context
     * but don't inherently have one.
     *
     * @return application context
     */
    public static Context getAppContext() {
        return mAppContext;
    }
}
