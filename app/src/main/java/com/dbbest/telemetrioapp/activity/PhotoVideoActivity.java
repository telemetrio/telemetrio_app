package com.dbbest.telemetrioapp.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.imageloader.TelemetrioUIL;
import com.dbbest.telemetrioapp.model.Photo;
import com.dbbest.telemetrioapp.model.SetDescription;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.dbbest.telemetrioapp.util.ShareUtils;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;

import java.io.File;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PhotoVideoActivity extends FragmentActivity {

    static final String[] EVENTS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Photos._ID,
            TelemetrioContract.Photos.PHOTO_ID,
            TelemetrioContract.Photos.PHOTO_IMAGE,
            TelemetrioContract.Photos.PHOTO_CLUB_ID,
            TelemetrioContract.Photos.PHOTO_TEAM_ID,
            TelemetrioContract.Photos.PHOTO_USER_ID,
            TelemetrioContract.Photos.PHOTO_FILE,
            TelemetrioContract.Photos.PHOTO_DESCRIPTION,
            TelemetrioContract.Photos.PHOTO_THUMBNAIL,
            TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS
    };

    private static final String TAG = "PhotoVideoActivity";
    private static final String ITEM_ID = "ItemId";
    private boolean isEdit = true;

    private SharedPreference sharedPreference;

    private Button delete_btn;
    private ProgressBar mProgressBar;
    private TextView description;
    private EditText description_edit;
    private Button edit_btn;
    private String mPhotoId;
    private ImageButton toolbar_share_button;
    private int uploadStatus = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.photo_video_activity);

        toolbar_share_button = (ImageButton)findViewById(R.id.toolbar_share_button);
        View backButton = findViewById(R.id.toolbar_left_button);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoVideoActivity.this.onBackPressed();
            }
        });

        sharedPreference = new SharedPreference(getBaseContext());


        delete_btn = (Button) findViewById(R.id.delete_btn);
        edit_btn = (Button) findViewById(R.id.edit_btn);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_indicator);
        final ImageView imageView = (ImageView) findViewById(R.id.image);
        description = (TextView) findViewById(R.id.description);
        description_edit = (EditText) findViewById(R.id.description_edit);

        final Uri baseUri = TelemetrioContract.Photos.CONTENT_URI;
        String WHERE =  TelemetrioContract.Photos.PHOTO_ID + "=?";
        String[] ARGS =  {getIntent().getStringExtra(ITEM_ID)};
        Cursor cursor = getContentResolver().query(baseUri, EVENTS_SUMMARY_PROJECTION, WHERE, ARGS, TelemetrioContract.Photos.DEFAULT_SORT);
        int photoId = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_ID);
        int fileIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_FILE);
        int photoIsImageIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_IMAGE);
        int clubIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_CLUB_ID);
        int teamIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_TEAM_ID);
        int userIdIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_USER_ID);
        int photoThumbIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_THUMBNAIL);
        int uploadStatusIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS);
        int descriptionIndex = cursor.getColumnIndex(TelemetrioContract.Photos.PHOTO_DESCRIPTION);


        final Photo photo = new Photo();

        while (cursor.moveToNext()) {
            uploadStatus = cursor.getInt(uploadStatusIndex);
            photo.fileId = cursor.getLong(photoId);
            photo.fileUrl = cursor.getString(fileIndex);
            photo.userId = cursor.getLong(userIdIndex);
            photo.clubId = cursor.getLong(clubIdIndex);
            photo.teamId = cursor.getLong(teamIdIndex);
            photo.description = cursor.getString(descriptionIndex);
            photo.thumbnail = cursor.getString(photoThumbIndex);
            photo.status = uploadStatus;
        }
        cursor.close();

        mPhotoId =  String.valueOf(photo.fileId);

        delete_btn.setOnClickListener(null);
        delete_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                deleteMedia(String.valueOf(photo.fileId), String.valueOf(photo.status));
            }
        });

        edit_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                editDescription();
            }
        });

        if (null != photo.description) {
            description.setText(photo.description);
            description_edit.setText(photo.description);
        }


        String displayUri =  getDisplayUri(uploadStatus, photo);
        TelemetrioUIL.getInstance().displayImage(displayUri, imageView, TelemetrioUIL.getDisplayOptions(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(final String s, View view, Bitmap bitmap) {
                Log.d("onLoadingComplete", "");
                Log.d("onLoadingComplete", "2");
                Log.d("onLoadingComplete", "3");
                final File file = DiskCacheUtils.findInCache(s, TelemetrioUIL.getInstance().getDiskCache());

                toolbar_share_button.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        ShareUtils.share(PhotoVideoActivity.this, file);
                    }
                });

                toolbar_share_button.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });


    }

    private String getDisplayUri(int uploadStatus, Photo photo){
        String displayUri = null;
        if(uploadStatus == Photo.SERVER_FILE_STATUS) {
            displayUri = photo.fileUrl;
        }
        if(uploadStatus == Photo.NOT_UPLOADED_STATUS) {
            displayUri =  "file://" + photo.fileUrl;
        }
        Log.d("PhotoVideoActivity", "getDisplayUri " + displayUri);
        return displayUri;
    }

    private void editDescription() {
        if(isEdit) {
            description_edit.setVisibility(View.VISIBLE);
            description.setVisibility(View.GONE);
            edit_btn.setText(getString(R.string.save_btn));

        } else {
            mProgressBar.setVisibility(View.VISIBLE);

            description_edit.setVisibility(View.GONE);
            description.setVisibility(View.VISIBLE);
            edit_btn.setText(getString(R.string.btn_edit));

            String photoDescription = String.valueOf(description_edit.getText());

            if (0 != uploadStatus) {

                TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).
                        setPhotoDescription(mPhotoId, photoDescription, new Callback<SetDescription>() {

                            @Override
                            public void success(SetDescription setDescription, Response response) {
                                description.setText(setDescription.description);
                                mProgressBar.setVisibility(View.INVISIBLE);

                                final Uri baseUri = TelemetrioContract.Photos.CONTENT_URI;

                                ContentValues values = new ContentValues();
                                values.put(TelemetrioContract.Photos.PHOTO_DESCRIPTION, setDescription.description);
                                String where = TelemetrioContract.Photos.PHOTO_ID + "=?";
                                String[] args = {mPhotoId};
                                getContentResolver().update(baseUri, values, where, args);


                            }

                            @Override
                            public void failure(RetrofitError error) {
                                mProgressBar.setVisibility(View.INVISIBLE);

                            }
                        });
            } else {
                final Uri baseUri = TelemetrioContract.Photos.CONTENT_URI;
                ContentValues values = new ContentValues();
                values.put(TelemetrioContract.Photos.PHOTO_DESCRIPTION, photoDescription);
                String where = TelemetrioContract.Photos.PHOTO_ID + "=?";
                String[] args = {mPhotoId};
                getContentResolver().update(baseUri, values, where, args);
                mProgressBar.setVisibility(View.INVISIBLE);

            }


        }
        isEdit = !isEdit;
    }

    public static void start(Activity activity, String id){
        Intent intent = new Intent(activity, PhotoVideoActivity.class);
        intent.putExtra(ITEM_ID, id);
        activity.startActivity(intent);
    }

    private void deleteMedia(final String media_id, final String status) {
        if (status.equals("1")) {
            mProgressBar.setVisibility(View.VISIBLE);
            TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).
                    deleteMedia(media_id, new Callback<String[]>() {


                        @Override
                        public void success(String[] serverResp, Response response) {
//                        showToast(response.getBody().toString());
//                        showToast(serverResp[0]);
                            mProgressBar.setVisibility(View.INVISIBLE);

                            String where = TelemetrioContract.Photos.PHOTO_ID + "=?";
                            String[] args = {media_id};

                            getContentResolver().delete(TelemetrioContract.Photos.CONTENT_URI, where, args);

                            PhotoVideoActivity.this.onBackPressed();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            mProgressBar.setVisibility(View.INVISIBLE);
                            showToast(error.getMessage());
                        }
                    });
        } else {
            String where = TelemetrioContract.Photos.PHOTO_ID + "=?";
            String[] args = {media_id};

            getContentResolver().delete(TelemetrioContract.Photos.CONTENT_URI, where, args);

            PhotoVideoActivity.this.onBackPressed();
        }

    }


    private void showToast(String message) {
        Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
