package com.dbbest.telemetrioapp.imageloader;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class UILProgressListener implements ImageLoadingListener {
    ProgressBar spinner;
    View view;

    public UILProgressListener(ProgressBar spinner) {
        this.spinner = spinner;
    }

    public UILProgressListener(ProgressBar spinner, View view) {
        this.spinner = spinner;
        this.view = view;
    }

    @Override
    public void onLoadingStarted(String s, View view) {
        showProgress();
    }

    @Override
    public void onLoadingFailed(String s, View view, FailReason failReason) {
    }

    @Override
    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
        hideProgress();
    }

    @Override
    public void onLoadingCancelled(String s, View view) {

    }

    private void showProgress(){
        spinner.setVisibility(View.VISIBLE);
        if(view != null) view.setVisibility(View.GONE);
    }

    private void hideProgress(){
        spinner.setVisibility(View.GONE);
        if(view != null) view.setVisibility(View.VISIBLE);
    }

}
