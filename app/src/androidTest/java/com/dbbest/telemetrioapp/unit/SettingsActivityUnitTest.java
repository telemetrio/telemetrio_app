package com.dbbest.telemetrioapp.unit;

import android.content.Intent;
import android.test.suitebuilder.annotation.SmallTest;
import android.view.View;
import android.widget.Button;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.activity.SettingsActivity;
import com.nostra13.universalimageloader.core.ImageLoader;


public class SettingsActivityUnitTest extends
        android.test.ActivityUnitTestCase<SettingsActivity> {

    private int buttonId;
    private SettingsActivity activity;

    public SettingsActivityUnitTest() {
        super(SettingsActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                SettingsActivity.class);
        startActivity(intent, null, null);
        activity = getActivity();
    }

//    @SmallTest
//    public void testLayout() {
//        buttonId = R.id.row_05;
//        assertNotNull(activity.findViewById(buttonId));
//    }
//
//    public void testIntentTriggerViaOnClick() {
//        buttonId = R.id.row_05;
//        Button view = (Button) activity.findViewById(buttonId);
//        assertNotNull("Button not allowed to be null", view);
//
//        view.performClick();
//
//        // TouchUtils cannot be used, only allowed in
//        // InstrumentationTestCase or ActivityInstrumentationTestCase2
//
//        // Check the intent which was started
//        Intent triggeredIntent = getStartedActivityIntent();
//        assertNotNull("Intent was null", triggeredIntent);
//        String data = triggeredIntent.getExtras().getString("URL");
//
//        assertEquals("Incorrect data passed via the intent",
//                "faq", data);
//    }


}
