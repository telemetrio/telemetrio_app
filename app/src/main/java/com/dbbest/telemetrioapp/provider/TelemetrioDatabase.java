/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dbbest.telemetrioapp.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.dbbest.telemetrioapp.provider.TelemetrioContract.EventsColumns;
import com.dbbest.telemetrioapp.provider.TelemetrioContract.PhotosColumns;
import com.dbbest.telemetrioapp.provider.TelemetrioContract.BookmarkColumns;

/**
 * Helper for managing {@link android.database.sqlite.SQLiteDatabase} that stores data for
 * {@link TelemetrioProvider}.
 */
public class TelemetrioDatabase extends SQLiteOpenHelper {
    private static final String TAG = "DB";

    private static final String DATABASE_NAME = "telemetrio.db";

    // NOTE: carefully update onUpgrade() when bumping database versions to make
    // sure user data is saved.

    private static final int CUR_DATABASE_VERSION = 21;

    interface Tables {
        String EVENTS = "events";
        String PHOTOS = "photos";
        String BOOKMARKS = "bookmarks";
    }

    public TelemetrioDatabase(Context context) {
        super(context, DATABASE_NAME, null, CUR_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + Tables.EVENTS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + EventsColumns.EVENT_LEAGUE_ID + " INTEGER  ,"
                + EventsColumns.EVENT_LEAGUE_NAME + " TEXT ,"
                + EventsColumns.EVENT_COUNTRY + " TEXT,"
                + EventsColumns.EVENT_TIME_BEGIN + " INTEGER,"
                + EventsColumns.EVENT_TIME_END + " INTEGER,"
                + EventsColumns.EVENT_FIELD_ID + " INTEGER,"
                + EventsColumns.EVENT_FIELD_NAME + " TEXT,"
                + EventsColumns.EVENT_ID_GAME + " TEXT,"

                + EventsColumns.EVENT_FIRST_TEAM_ID + " INTEGER NOT NULL,"
                + EventsColumns.EVENT_FIRST_TEAM_NAME + " TEXT NOT NULL,"
                + EventsColumns.EVENT_FIRST_TEAM_LOGO + " TEXT NOT NULL,"
                + EventsColumns.EVENT_FIRST_TEAM_GOALS + " INTEGER NOT NULL,"

                + EventsColumns.EVENT_TYPE + " INTEGER ,"
                + EventsColumns.EVENT_TODAY + " INTEGER ,"

                + EventsColumns.EVENT_SECOND_TEAM_ID + " INTEGER NOT NULL,"
                + EventsColumns.EVENT_SECOND_TEAM_NAME + " TEXT NOT NULL,"
                + EventsColumns.EVENT_SECOND_TEAM_LOGO + " TEXT NOT NULL,"
                + EventsColumns.EVENT_SECOND_TEAM_GOALS + " INTEGER NOT NULL)");

        db.execSQL("CREATE TABLE " + Tables.PHOTOS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PhotosColumns.PHOTO_ID + " INTEGER NOT NULL,"
                + PhotosColumns.PHOTO_LOCAL_ID + " INTEGER ,"
                + PhotosColumns.PHOTO_IMAGE + " INTEGER NOT NULL,"
                + PhotosColumns.PHOTO_FILE + " TEXT NOT NULL,"
                + PhotosColumns.PHOTO_CLUB_ID + " INTEGER NOT NULL,"
                + PhotosColumns.PHOTO_TEAM_ID + " INTEGER NOT NULL,"
                + PhotosColumns.PHOTO_USER_ID + " INTEGER NOT NULL,"
                + PhotosColumns.PHOTO_THUMBNAIL + " TEXT NOT NULL,"
                + PhotosColumns.PHOTO_DESCRIPTION + " TEXT ,"
                + PhotosColumns.PHOTO_UPLOAD_STATUS + " INTEGER NOT NULL)");

        db.execSQL("CREATE TABLE " + Tables.BOOKMARKS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + BookmarkColumns.BOOKMARK_ID + " INTEGER NOT NULL,"
                + BookmarkColumns.BOOKMARK_EVENT_ID + " TEXT NOT NULL,"
                + BookmarkColumns.BOOKMARK_ACTION + " INTEGER NOT NULL,"
                + BookmarkColumns.BOOKMARK_TIME + " TEXT NOT NULL,"
                + BookmarkColumns.BOOKMARK_LAT + " TEXT ,"
                + BookmarkColumns.BOOKMARK_LONG + " TEXT ,"
                + BookmarkColumns.BOOKMARK_ACCURACY + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

        // at this point, we ran out of upgrade logic, so if we are still at the wrong
        // version, we have no choice but to delete everything and create everything again.
        if (newVersion != oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Tables.EVENTS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.PHOTOS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.BOOKMARKS);
            onCreate(db);
        }

    }
}
