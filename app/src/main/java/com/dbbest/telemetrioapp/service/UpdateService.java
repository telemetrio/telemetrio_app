package com.dbbest.telemetrioapp.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.Log;

import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.model.Event;
import com.dbbest.telemetrioapp.model.GetConfiguration;
import com.dbbest.telemetrioapp.model.Photo;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class UpdateService extends IntentService {
    private static final String ACTION_FETCH_NEW_EVENTS = "com.dbbest.telemetrioapp.service.action.FETCH_NEW_EVENTS";
    private static final String ACTION_FETCH_NEW_PHOTOS = "com.dbbest.telemetrioapp.service.action.FETCH_NEW_PHOTOS";
    private static final String ACTION_FETCH_CONFIGURATIONS = "com.dbbest.telemetrioapp.service.action.FETCH_CONFIG";

    private static final String EXTRA_RESULT_RECEIVER = "com.dbbest.telemetrioapp.service.extra.result_receiver";
    private static final String EXTRA_USER_ID = "com.dbbest.telemetrioapp.service.extra.user_id";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFetchNewEvents(Context context, String userId, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, UpdateService.class);
        intent.setAction(ACTION_FETCH_NEW_EVENTS);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_RESULT_RECEIVER, resultReceiver);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFetchNewPhotos(Context context, String userId, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, UpdateService.class);
        intent.setAction(ACTION_FETCH_NEW_PHOTOS);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_RESULT_RECEIVER, resultReceiver);
        context.startService(intent);
    }

    public static void startActionFetchConfigurations(Context context, String userId, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, UpdateService.class);
        intent.setAction(ACTION_FETCH_CONFIGURATIONS);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_RESULT_RECEIVER, resultReceiver);
        context.startService(intent);
    }

    public UpdateService() {
        super("UpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_NEW_EVENTS.equals(action)) {
                final String userId = intent.getStringExtra(EXTRA_USER_ID);
                final ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
                if (null != resultReceiver) {
                    handleUpdateEvents(resultReceiver, userId);
                }
            }
            if (ACTION_FETCH_NEW_PHOTOS.equals(action)) {
                final String userId = intent.getStringExtra(EXTRA_USER_ID);
                final ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
                if (null != resultReceiver) {
                    handleUpdatePhotos(resultReceiver, userId);
                }
            }
//            if (ACTION_FETCH_NEW_PHOTOS.equals(action)) {
//                final String userId = intent.getStringExtra(EXTRA_USER_ID);
//                final ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
//                if (null != resultReceiver) {
//                    handleUpdateConfigurations(resultReceiver, userId);
//                }
//            }
        }
    }

    /**
     * Handle ACTION_FETCH_NEW_EVENTS in the provided background thread with the provided
     * parameters.
     */
    private void handleUpdateEvents(final ResultReceiver resultReceiver, String userId) {
        // produce the necessary content provider operations
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        //"time_begin": "2014-09-27 01:30:00",

        final SharedPreference sharedPreference = new SharedPreference(UpdateService.this);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

        try {
            List<Event> events = TelemetryApplication.getRestServiceWithConverter(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL, new GsonConverter(gson)).
                    listEvents(userId, "0");

            Uri uri = TelemetrioContract.Events.CONTENT_URI;

            // The list of events is not large, so for simplicity we delete all of them and repopulate
            batch.add(ContentProviderOperation.newDelete(uri).build());

            if (events.size() > 0) {
                for (Event event : events) {
                    ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(uri);
                    builder.withValue(TelemetrioContract.Events.EVENT_COUNTRY, event.country);
                    builder.withValue(TelemetrioContract.Events.EVENT_FIELD_ID, event.fieldId);
                    if (null != event.fieldName) {
                        builder.withValue(TelemetrioContract.Events.EVENT_FIELD_NAME, event.fieldName);
                    }
//                if (null != event.leagueId) {
                    builder.withValue(TelemetrioContract.Events.EVENT_LEAGUE_ID, event.leagueId);
//                }
                    if (null != event.leagueName) {
                        builder.withValue(TelemetrioContract.Events.EVENT_LEAGUE_NAME, event.leagueName);
                    }
                    builder.withValue(TelemetrioContract.Events.EVENT_TIME_BEGIN, event.startTime.getTime());
                    if (null != event.endTime) {
                        builder.withValue(TelemetrioContract.Events.EVENT_TIME_END, event.endTime.getTime());
                    } else {
                        builder.withValue(TelemetrioContract.Events.EVENT_TIME_END, 1);

                    }

                    builder.withValue(TelemetrioContract.Events.EVENT_ID_GAME, event.idGame);

                    builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_ID, event.getTeam(Event.FIRST_TEAM_KEY).teamId);
                    builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME, event.getTeam(Event.FIRST_TEAM_KEY).teamName);
                    builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_LOGO, event.getTeam(Event.FIRST_TEAM_KEY).logo);
                    builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_GOALS, event.getTeam(Event.FIRST_TEAM_KEY).goals);

                    builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_ID, event.getTeam(Event.SECOND_TEAM_KEY).teamId);
                    builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME, event.getTeam(Event.SECOND_TEAM_KEY).teamName);
                    builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_LOGO, event.getTeam(Event.SECOND_TEAM_KEY).logo);
                    builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_GOALS, event.getTeam(Event.SECOND_TEAM_KEY).goals);

                    batch.add(builder.build());
                }
            }

            try {
                int operations = batch.size();
                if (operations > 0) {
                    getContentResolver().applyBatch(TelemetrioContract.CONTENT_AUTHORITY, batch);
                }
                resultReceiver.send(Activity.RESULT_OK, Bundle.EMPTY);
            } catch (RemoteException | OperationApplicationException ex) {
                throw new RuntimeException("Error executing content provider batch operation", ex);
            }
        }
        catch (Exception e){
            Uri uri = TelemetrioContract.Events.CONTENT_URI;
            ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(uri);
            builder.withValue(TelemetrioContract.Events.EVENT_COUNTRY, "Ukraine");
            builder.withValue(TelemetrioContract.Events.EVENT_FIELD_ID, "1");
            builder.withValue(TelemetrioContract.Events.EVENT_FIELD_NAME, "Dynamo Stadium");
            builder.withValue(TelemetrioContract.Events.EVENT_LEAGUE_ID, "1");
            builder.withValue(TelemetrioContract.Events.EVENT_LEAGUE_NAME, "Champions League");
            builder.withValue(TelemetrioContract.Events.EVENT_TIME_BEGIN, 0);
            builder.withValue(TelemetrioContract.Events.EVENT_TIME_END, 1);

            builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_ID, 1);
            builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME, "Dynamo");
            builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_LOGO, "");
            builder.withValue(TelemetrioContract.Events.EVENT_FIRST_TEAM_GOALS, 1);

            builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_ID, 2);
            builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME, "Juventus");
            builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_LOGO, "");
            builder.withValue(TelemetrioContract.Events.EVENT_SECOND_TEAM_GOALS, 0);

            builder.withValue(TelemetrioContract.Events.EVENT_ID_GAME, 88);

            batch.add(builder.build());
            try {
                getContentResolver().applyBatch(TelemetrioContract.CONTENT_AUTHORITY, batch);
            } catch (RemoteException | OperationApplicationException ex) {
                throw new RuntimeException("Error executing content provider batch operation", ex);
            }

            Log.d("UpdateService", "handleUpdateEvents: " + e.getMessage());
            resultReceiver.send(Activity.RESULT_CANCELED, Bundle.EMPTY);
        }



    }

    private void handleUpdateConfigurations(ResultReceiver resultReceiver, String userId) {
        final SharedPreference sharedPreference = new SharedPreference(UpdateService.this);

        TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).getConfigurations(userId, new Callback<GetConfiguration>() {
            @Override
            public void success(GetConfiguration configuration, Response response) {
                if (null != configuration) {
                    if (null != configuration.emailWeekly) {
                        sharedPreference.setSettingParam_01(configuration.emailWeekly.state);
                    }
                    if (null != configuration.pushNotifications) {
                        sharedPreference.setSettingParam_02(configuration.pushNotifications.state);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleUpdatePhotos(ResultReceiver resultReceiver, String userId) {
        // produce the necessary content provider operations
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        SharedPreference sharedPreference = new SharedPreference(UpdateService.this);

        try {
            List<Photo> photos = TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).listPhotos(userId);

            Uri uri = TelemetrioContract.Photos.CONTENT_URI;

            // The list of photos is not large, so for simplicity we delete all of them and repopulate
            // This operation delete only photos received from server, because of selection 'photo_upload_status=-1'
            batch.add(ContentProviderOperation.newDelete(uri)
                    .withSelection(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS + "=?", new String[]{"1"})
                    .build());

            for (Photo photo : photos) {
                ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(uri);
                builder.withValue(TelemetrioContract.Photos.PHOTO_ID, photo.fileId);
                builder.withValue(TelemetrioContract.Photos.PHOTO_IMAGE, photo.image);
                builder.withValue(TelemetrioContract.Photos.PHOTO_DESCRIPTION, photo.description);
                builder.withValue(TelemetrioContract.Photos.PHOTO_FILE, photo.fileUrl);
                builder.withValue(TelemetrioContract.Photos.PHOTO_CLUB_ID, photo.clubId);
                builder.withValue(TelemetrioContract.Photos.PHOTO_TEAM_ID, photo.teamId);
                builder.withValue(TelemetrioContract.Photos.PHOTO_USER_ID, photo.userId);
                builder.withValue(TelemetrioContract.Photos.PHOTO_THUMBNAIL, photo.thumbnail);
                builder.withValue(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS, 1);
                batch.add(builder.build());
            }

            try {
                int operations = batch.size();
                if (operations > 0) {
                    getContentResolver().applyBatch(TelemetrioContract.CONTENT_AUTHORITY, batch);
                }
                if (null != resultReceiver) {
                    resultReceiver.send(Activity.RESULT_OK, Bundle.EMPTY);
                }
            } catch (RemoteException | OperationApplicationException ex) {
                throw new RuntimeException("Error executing content provider batch operation", ex);
            }
        }
        catch (Exception e){

            Log.d("UpdateService", "handleUpdatePhotos: " + e.getMessage());
            resultReceiver.send(Activity.RESULT_CANCELED, Bundle.EMPTY);
        }


    }
}
