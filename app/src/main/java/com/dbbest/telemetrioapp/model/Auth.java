package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrew Druk on 11/26/14.
 */
public class Auth {
    @SerializedName("key")
    public String key;

    @Override
    public String toString() {
        return "Auth{" +
                "key='" + key + '\'' +
                '}';
    }
}
