package com.dbbest.telemetrioapp.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Andrew Druk on 11/27/14.
 */
public class TelemetrioContract {

    public static final String CONTENT_AUTHORITY = "com.dbbest.telemetrioapp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final String PATH_EVENTS = "events";
    private static final String PATH_PHOTOS = "photos";
    private static final String PATH_BOOKMARK = "bookmarks";

    interface TeamColumns {
        String TEAM_ID = "team_id";
        String TEAM_ROLE_ID = "team_role_id";
        String TEAM_NAME = "team_name";
        String TEAM_ROLE_NAME = "team_role_name";
        String TEAM_LOGO = "team_logo";
    }

    interface BookmarkColumns {
        String BOOKMARK_ID = "bookmark_id";
        String BOOKMARK_EVENT_ID = "bookmark_event_id";
        String BOOKMARK_ACTION = "bookmark_action";
        String BOOKMARK_TIME = "bookmark_time";
        String BOOKMARK_LAT = "bookmark_location_lat";
        String BOOKMARK_LONG = "bookmark_location_long";
        String BOOKMARK_ACCURACY = "bookmark_location_accuracy";
    }

    interface EventsColumns {
        String EVENT_LEAGUE_ID = "event_league_id";
        String EVENT_LEAGUE_NAME = "event_league_name";
        String EVENT_COUNTRY = "event_country";
        String EVENT_TIME_BEGIN = "event_time_begin";
        String EVENT_TIME_END = "event_time_end";
        String EVENT_FIELD_ID = "event_field_id";
        String EVENT_FIELD_NAME = "event_field_name";

        String EVENT_ID_GAME = "event_id_game";


        String EVENT_FIRST_TEAM_ID = "event_first_team_id";
        String EVENT_FIRST_TEAM_NAME = "event_first_team_name";
        String EVENT_FIRST_TEAM_LOGO = "event_first_team_logo";
        String EVENT_FIRST_TEAM_GOALS = "event_first_team_goals";

        String EVENT_TYPE = "event_type";
        String EVENT_TODAY = "event_today";

        String EVENT_SECOND_TEAM_ID = "event_second_team_id";
        String EVENT_SECOND_TEAM_NAME = "event_second_team_name";
        String EVENT_SECOND_TEAM_LOGO = "event_second_team_logo";
        String EVENT_SECOND_TEAM_GOALS = "event_second_team_goals";

    }

    interface SettingColumns {
        String SETTING_USER_ID = "setting_user_id";
        String SETTING_SETTING = "setting_setting";
        String SETTING_STATE = "setting_state";

    }

    interface PhotosColumns {
        String PHOTO_ID = "photo_id";
        String PHOTO_LOCAL_ID = "photo_local_id";
        String PHOTO_IMAGE = "photo_image";
        String PHOTO_FILE = "photo_file";
        String PHOTO_USER_ID = "photo_user_id";
        String PHOTO_CLUB_ID = "photo_club_id";
        String PHOTO_TEAM_ID = "photo_team_id";
        String PHOTO_THUMBNAIL = "photo_thumbnail";
        String PHOTO_DESCRIPTION = "photo_description";
        String PHOTO_UPLOAD_STATUS = "photo_upload_status"; //flag for uploading {-1,0,1} {server media file, not uploaded yet, uploaded}
    }

    /**
     *  Photos in gallery
     */
    public static class Bookmarks implements BookmarkColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_BOOKMARK).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.telemetrioapp.bookmark";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.telemetrioapp.bookmark";

        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = BookmarkColumns.BOOKMARK_ID + " ASC";


        /** Build {@link Uri} for requested {@link #_ID}. */
        public static Uri buildBookmarkUri(String bookmarkId) {
            return CONTENT_URI.buildUpon().appendPath(bookmarkId).build();
        }

        /** Read {@link #_ID} from {@link com.dbbest.telemetrioapp.provider.TelemetrioContract.Photos} {@link Uri}. */
        public static String geBookmarkId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }


    /**
     *  Events in scheduler
     */
    public static class Events implements EventsColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.telemetrioapp.event";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.telemetrioapp.event";

        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = EventsColumns.EVENT_TIME_BEGIN + " DESC";

        /** Build {@link Uri} for requested {@link #_ID}. */
        public static Uri buildEventUri(String eventId) {
            return CONTENT_URI.buildUpon().appendPath(eventId).build();
        }

        /** Read {@link #_ID} from {@link com.dbbest.telemetrioapp.provider.TelemetrioContract.Events} {@link Uri}. */
        public static String geEventId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    /**
     *  Events in scheduler
     */
    public static class Setting implements SettingColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.telemetrioapp.setting";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.telemetrioapp.setting";



    }

    /**
     *  Photos in gallery
     */
    public static class Photos implements PhotosColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PHOTOS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.telemetrioapp.photo";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.telemetrioapp.photo";

        /** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = PhotosColumns.PHOTO_ID + " DESC";

        /** EVENT "ORDER BY" clause. */
        public static final String EVENT_SORT = PhotosColumns.PHOTO_TEAM_ID + " ASC";


        /** Build {@link Uri} for requested {@link #_ID}. */
        public static Uri buildPhotoUri(String photoId) {
            return CONTENT_URI.buildUpon().appendPath(photoId).build();
        }

        /** Read {@link #_ID} from {@link com.dbbest.telemetrioapp.provider.TelemetrioContract.Photos} {@link Uri}. */
        public static String gePhotoId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

}
