package com.dbbest.telemetrioapp.telemetriorest;

import android.net.http.AndroidHttpClient;
import android.util.Log;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedString;

/**
 * Created by Andrew Druk on 11/26/14.
 */
public class TelemetrioRestClient implements Client {

    private AndroidHttpClient httpClient;
    private HttpContext credContext;

    public TelemetrioRestClient(){
        String url = "https://www.telemetrio.com/api/";
        String username = "admin";
        String password = "SPgH4sa0gEd3jc0xm1oHfyqVHqXF2R4r8gTecHM2";
        httpClient = AndroidHttpClient.newInstance("test user agent");
        try {
            URL urlObj = new URL(url);
            AuthScope scope = new AuthScope(urlObj.getHost(), urlObj.getPort());
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);

            CredentialsProvider cp = new BasicCredentialsProvider();
            cp.setCredentials(scope, creds);
            credContext = new BasicHttpContext();
            credContext.setAttribute(ClientContext.CREDS_PROVIDER, cp);
        }
        catch (Exception e){
            throw new RuntimeException("TelemetrioRestClient construct exception");
        }
    }

    @Override
    public Response execute(Request request) throws IOException {
        // We're getting a Retrofit request, but we need to execute an Apache
        // HttpUriRequest instead. Use the info in the Retrofit request to create
        // an Apache HttpUriRequest.

        URL urlObj = new URL(request.getUrl());
        HttpHost host = new HttpHost(urlObj.getHost(), urlObj.getPort(), urlObj.getProtocol());

        String method = request.getMethod();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (request.getBody() != null) {
            request.getBody().writeTo(bos);
        }
        String body = new String(bos.toByteArray());

        HttpUriRequest wrappedRequest;
        switch (method) {
            case "GET":
                wrappedRequest = new HttpGet(request.getUrl());
                break;
            case "POST":
                wrappedRequest = new HttpPost(request.getUrl());
                ((HttpPost) wrappedRequest).setEntity(new StringEntity(body));
                break;
            case "PUT":
                wrappedRequest = new HttpPut(request.getUrl());
                ((HttpPut) wrappedRequest).setEntity(new StringEntity(body));
                break;
            case "DELETE":
                wrappedRequest = new HttpDelete(request.getUrl());
                break;
            default:
                throw new AssertionError("HTTP operation not supported.");
        }

        wrappedRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
        wrappedRequest.addHeader("X-API-KEY", "ecf8cdbe7d082ed4494c3b74e3be3264d483565a");

        HttpResponse response = httpClient.execute(host, wrappedRequest, credContext);

        //Convert apache http response to retrofit response
        List<Header> headersList = new LinkedList<>();
        org.apache.http.Header[] headers = response.getAllHeaders();
        for (org.apache.http.Header header : headers) {
            headersList.add(new retrofit.client.Header(header.getName(), header.getValue()));
        }

        Response retrofitResponce = new Response(request.getUrl(),
                response.getStatusLine().getStatusCode(),
                response.getStatusLine().getReasonPhrase(),
                headersList,
                new TypedString(EntityUtils.toString(response.getEntity())));
        Log.d("RestClient", retrofitResponce.getBody().toString());

        httpClient.close();

        return retrofitResponce;
    }
}
