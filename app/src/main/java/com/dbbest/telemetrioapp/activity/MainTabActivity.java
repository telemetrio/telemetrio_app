package com.dbbest.telemetrioapp.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.view.TabBookmarkFragment;
import com.dbbest.telemetrioapp.view.TabGalleryFragment;
import com.dbbest.telemetrioapp.view.TabScheduleFragment;

import java.util.Locale;

public class MainTabActivity extends ActionBarActivity implements ViewPager.OnPageChangeListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TextView mTitle;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TextView tab_0;
    private TextView tab_1;
    private TextView tab_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);

        mTitle = (TextView) findViewById(R.id.toolbar_title);

        View toolbar_right_button = findViewById(R.id.toolbar_right_button);
        toolbar_right_button.setVisibility(View.VISIBLE);

        toolbar_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent eventDetail = new Intent(MainTabActivity.this, SettingsActivity.class);
                startActivity(eventDetail);
            }
        });

//
        View backButton = findViewById(R.id.toolbar_left_button);
//        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainTabActivity.this.onBackPressed();
            }
        });

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tab_0 = (TextView) findViewById(R.id.tab_0);
        tab_1 = (TextView) findViewById(R.id.tab_1);
        tab_2 = (TextView) findViewById(R.id.tab_2);

        tab_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });
        tab_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
            }
        });
//        tab_2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mViewPager.setCurrentItem(2);
//            }
//        });

        tab_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent eventDetail = new Intent(MainTabActivity.this, BookmarkerActivity.class);
            eventDetail.putExtra("position", 12);
            startActivity(eventDetail);
            }
        });


        mViewPager.setCurrentItem(0);
        onPageSelected(0);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(this);

//        Intent eventDetail = new Intent(MainTabActivity.this, BookmarkerActivity.class);
//        eventDetail.putExtra("position", 12);
//        startActivity(eventDetail);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTitle.setText(mSectionsPagerAdapter.getPageTitle(position));

        switch (position) {
            case 0: {

                tab_0.setTextColor(Color.parseColor("#FFFFFF"));
                tab_1.setTextColor(Color.parseColor("#8990A0"));
                tab_2.setTextColor(Color.parseColor("#8990A0"));

                tab_0.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_select));
                tab_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));
                tab_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));

            }
            break;

            case 1: {

                tab_0.setTextColor(Color.parseColor("#8990A0"));
                tab_1.setTextColor(Color.parseColor("#FFFFFF"));
                tab_2.setTextColor(Color.parseColor("#8990A0"));

                tab_0.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));
                tab_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_select));
                tab_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));

            }
            break;
            case 2: {

                tab_0.setTextColor(Color.parseColor("#8990A0"));
                tab_1.setTextColor(Color.parseColor("#8990A0"));
                tab_2.setTextColor(Color.parseColor("#FFFFFF"));

                tab_0.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));
                tab_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_back));
                tab_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.maintab_bar_select));

            }
            break;
            default:
                break;

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).


            switch (position) {
                case 0:
                    return TabScheduleFragment.newInstance();
                case 1:
                    return TabGalleryFragment.newInstance();
                case 2:
                    return TabBookmarkFragment.newInstance();
                default:
                    throw new IllegalArgumentException("Unknown tab index");
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.schedule).toUpperCase(l);
                case 1:
                    return getString(R.string.gallery).toUpperCase(l);
                case 2:
                    return getString(R.string.bookmark).toUpperCase(l);
            }
            return null;
        }
    }



}
