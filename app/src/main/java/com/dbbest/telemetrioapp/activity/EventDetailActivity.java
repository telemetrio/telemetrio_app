package com.dbbest.telemetrioapp.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EventDetailActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final String TAG = "EventDetailActivity";

    public static final int REQUEST_CODE_RECORD_MEDIA = 1;

    private Context mContext;
    private FragmentActivityBackStack stack;
    private Integer mPosition;
    private static final int URL_EVENT_LOADER = 0;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_HEADER = 1;

    private TextView row_event_type;
    private TextView row_event_teams;
    private TextView row_event_name_event;
    private TextView row_event_name_number;
    private TextView row_event_day_type;
    private TextView row_event_txt_location;
    private TextView row_event_txt_date;
    private TextView row_event_txt_time;
    private ImageView row_event_icon_state;
    private TextView row_event_txt_state;
    private ImageView btn_bookmark;
    private String mEventId;

    private String mUserId;
    private SharedPreference sharedPreference;
    private ImageView btn_camera;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.event_detail_activity);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText(getString(R.string.event).toUpperCase());
        View backButton = findViewById(R.id.toolbar_left_button);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventDetailActivity.this.onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mPosition = bundle.getInt("position");
        }

        sharedPreference = new SharedPreference(getBaseContext());
        mUserId = sharedPreference.getUserID();


        row_event_type = (TextView)findViewById(R.id.row_event_type);
        row_event_teams = (TextView)findViewById(R.id.row_event_teams);
        row_event_name_event = (TextView)findViewById(R.id.row_event_name_event);

        row_event_name_number = (TextView)findViewById(R.id.row_event_name_number);
        row_event_day_type = (TextView)findViewById(R.id.row_event_day_type);


        row_event_txt_location = (TextView)findViewById(R.id.row_event_txt_location);
        row_event_txt_date = (TextView)findViewById(R.id.row_event_txt_date);
        row_event_txt_time = (TextView)findViewById(R.id.row_event_txt_time);

        row_event_icon_state = (ImageView)findViewById(R.id.row_event_icon_state);
        row_event_txt_state = (TextView)findViewById(R.id.row_event_txt_state);

        btn_bookmark = (ImageView)findViewById(R.id.btn_bookmark);
//        if(null != mPosition) {
//            btn_bookmark.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent bookmarkActivity = new Intent(EventDetailActivity.this, BookmarkerActivity.class);
//                    bookmarkActivity.putExtra("position", mPosition);
//                    startActivity(bookmarkActivity);
//                }
//            });
//
//        }

        btn_camera = (ImageView)findViewById(R.id.btn_camera);
//        if(null != mPosition) {
//            btn_camera.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    File mSaveDirectory = getExternalFilesDir("Media");
//                    RecordingActivity.start(EventDetailActivity.this, mSaveDirectory.getAbsolutePath(), mEventId, REQUEST_CODE_RECORD_MEDIA);
//                }
//            });
//
//        }

        getSupportLoaderManager().initLoader(0, null, this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_RECORD_MEDIA){
            if (resultCode == Activity.RESULT_OK) {

                Boolean isVideo = data.getBooleanExtra(RecordingActivity.KEY_IS_VIDEO, false);
                String path = data.getStringExtra(RecordingActivity.KEY_RESULT_FILE_PATH);

                String photoId = TelemetryApplication.randomString(5);

                //TODO: fill record with correct data
                ContentValues values = new ContentValues();
                values.put(TelemetrioContract.Photos.PHOTO_ID, photoId);
                values.put(TelemetrioContract.Photos.PHOTO_LOCAL_ID, photoId);
                values.put(TelemetrioContract.Photos.PHOTO_IMAGE, isVideo ? 0 : 1);
                values.put(TelemetrioContract.Photos.PHOTO_FILE, path);
                values.put(TelemetrioContract.Photos.PHOTO_THUMBNAIL, path);
                values.put(TelemetrioContract.Photos.PHOTO_USER_ID, mUserId);
                values.put(TelemetrioContract.Photos.PHOTO_CLUB_ID, "13");
                values.put(TelemetrioContract.Photos.PHOTO_TEAM_ID, "13");
                values.put(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS, "0");
                getContentResolver().insert(TelemetrioContract.Photos.CONTENT_URI, values);
                Toast.makeText(this, ((isVideo) ? "Video " : "Photo ") + " was saved at " + path, Toast.LENGTH_SHORT).show();

                File mSaveDirectory = getExternalFilesDir("Media");
                Intent intent = new Intent(EventDetailActivity.this, RecordingActivity.class);
                intent.putExtra(RecordingActivity.KEY_SAVE_PATH, mSaveDirectory.getAbsolutePath());
                startActivityForResult(intent, REQUEST_CODE_RECORD_MEDIA);
            }
        }
    }

    @Override
    public void onResume() {
		super.onResume();
	}

	@Override
    public void onPause() {
		super.onPause();

	}

	private void showToast(String message) {
			Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
	}

    // These are the Contacts rows that we will retrieve.
    static final String[] EVENTS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Events._ID,
            TelemetrioContract.Events.EVENT_LEAGUE_NAME,
            TelemetrioContract.Events.EVENT_FIELD_NAME,
            TelemetrioContract.Events.EVENT_TIME_BEGIN,
            TelemetrioContract.Events.EVENT_TIME_END,
            TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME,
            TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME,
            TelemetrioContract.Events.EVENT_FIELD_ID,
            TelemetrioContract.Events.EVENT_ID_GAME,

            TelemetrioContract.Events.EVENT_TYPE,
            TelemetrioContract.Events.EVENT_TODAY,

    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.

        if (id == URL_EVENT_LOADER) {
            Uri baseUri = TelemetrioContract.Events.CONTENT_URI;

//            if (null != mPosition) {
//                String SELECTION = TelemetrioContract.Events._ID + "=?";
//                List<String> selectionArgs = new ArrayList<String>();
//                selectionArgs.add(mPosition.toString());
//
//
//                final String[] SELECTION_ARGS = new String[selectionArgs.size()];
//                selectionArgs.toArray(SELECTION_ARGS);
//                return new CursorLoader(this, baseUri,
//                        EVENTS_SUMMARY_PROJECTION, SELECTION, SELECTION_ARGS,
//                        TelemetrioContract.Events.DEFAULT_SORT);
//            } else {


                return new CursorLoader(this, baseUri,
                        EVENTS_SUMMARY_PROJECTION, null, null,
                        TelemetrioContract.Events.DEFAULT_SORT);
//            }
        }
        else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, final Cursor cursor) {

        if (cursor.moveToPosition(mPosition)) {

            String dateFormat = "dd, MMM yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

            String timeFormat = "HH:mm";
            SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

            int row_today = getItemIsToday(cursor.getPosition(), cursor);
//            int row_type = getItemViewType(cursor.getPosition(), cursor);




            if(0 == row_today) {
                row_event_icon_state.setVisibility(View.GONE);
                row_event_txt_state.setVisibility(View.GONE);

            } else {
                row_event_icon_state.setVisibility(View.VISIBLE);
                row_event_txt_state.setVisibility(View.VISIBLE);


                if(2 == row_today) {  //current
                    row_event_icon_state.setImageResource(R.drawable.row_event_status_online);
                    row_event_txt_state.setText(R.string.row_event_txt_state_1);
                } else if((1 == row_today) || (4 == row_today)) {   // today | new
                    row_event_icon_state.setImageResource(R.drawable.row_event_status_offline);
                    row_event_txt_state.setText(R.string.row_event_txt_state_2);
                } else {
                    row_event_icon_state.setVisibility(View.GONE);
                    row_event_txt_state.setVisibility(View.GONE);
                }
            }



            row_event_name_number.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_NAME)) + " " +
                            cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_ID))
            );

            row_event_type.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_LEAGUE_NAME)));

            row_event_teams.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME)) + " VS " +
                            cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME))
            );

            row_event_name_event.setText(cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_FIELD_NAME)));

            String eventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String eventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(eventTimeBegin));
            Date mEventTimeBegin = calEventTimeBegin.getTime();

            row_event_txt_date.setText( sdf.format(mEventTimeBegin ));

            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(eventTimeEnd));
            Date mEventTimeEnd = calEventTimeEnd.getTime();

            row_event_txt_time.setText( stf.format(mEventTimeBegin ) + " - " + stf.format(mEventTimeEnd ));

            mEventId = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_ID_GAME));

            btn_bookmark.setOnClickListener(null);
            btn_bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent bookmarkActivity = new Intent(EventDetailActivity.this, BookmarkerActivity.class);
//                    bookmarkActivity.putExtra("position", mPosition);
                    bookmarkActivity.putExtra("event_id", mEventId);
                    startActivity(bookmarkActivity);
                }
            });

            btn_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File mSaveDirectory = getExternalFilesDir("Media");
                    RecordingActivity.start(EventDetailActivity.this, mSaveDirectory.getAbsolutePath(), mEventId, REQUEST_CODE_RECORD_MEDIA);
                }
            });
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    public int getItemIsToday(int position, Cursor cursor) {
        int ret = 0;
        if(cursor.moveToPosition(position)) {
//            Event item = new Event();

            String sEventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String sEventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(sEventTimeBegin));

            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(sEventTimeEnd));

            Date eventTimeBegin = calEventTimeBegin.getTime();
            Date eventTimeEnd = calEventTimeEnd.getTime();

            String dateFormat = "yyyy MM dd";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

//        String timeFormat = "HH:mm";
//        SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

            Calendar currentTime = Calendar.getInstance();
            Date mCurrentTime = currentTime.getTime();

//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            // Create a calendar object with today date. Calendar is in java.util pakage.
            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DATE, -1);

            String dateFormatParse = "yyyy MM dd 00:01:01";
            SimpleDateFormat sdfYesterday = new SimpleDateFormat(dateFormatParse);

            Date mYesterday = calendar.getTime();
            String strYesterday = sdfYesterday.format(mYesterday).toString();


            calendar.add(Calendar.DATE, 1);

            mYesterday = calendar.getTime();
            String strTomorrow = sdfYesterday.format(mYesterday).toString();



            if (sdf.format(eventTimeBegin).equals(sdf.format(mCurrentTime))) {
                ret = 1;
            }
            if (eventTimeBegin.before(mCurrentTime) && eventTimeEnd.after(mCurrentTime)) {
                ret = 2;
            }
            try{
                Date parsed = sdf.parse(strYesterday);
                if (eventTimeEnd.before(parsed)) {
                    ret = 3;
                }
            } catch(Exception e){
                System.out.println(e.getMessage());
            }

            try{
                Date parsed = sdf.parse(strTomorrow);
                if (eventTimeBegin.after(parsed)) {
                    ret = 4;
                }
            } catch(Exception e){
                System.out.println(e.getMessage());
            }

        }
        return ret;
    }

    public int getItemIsToday2(int position, Cursor cursor) {
        int ret = 0;
        if(cursor.moveToPosition(position)) {
//            Event item = new Event();

            String sEventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String sEventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(sEventTimeBegin));

            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(sEventTimeEnd));

            Date eventTimeBegin = calEventTimeBegin.getTime();
            Date eventTimeEnd = calEventTimeEnd.getTime();

            String dateFormat = "yyyy MM dd";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

//        String timeFormat = "HH:mm";
//        SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

            Calendar currentTime = Calendar.getInstance();
            Date mCurrentTime = currentTime.getTime();


            if (sdf.format(eventTimeBegin).equals(sdf.format(mCurrentTime))) {
                ret = 1;
            }
            if (eventTimeBegin.before(mCurrentTime) && eventTimeEnd.after(mCurrentTime)) {
                ret = 2;
            }

        }
        return ret;
    }

    public int getItemViewType(int position, Cursor cursor) {
        int ret =  TYPE_NORMAL;
        if(0 == position) {
            ret =  TYPE_HEADER;
        }
        else {
            if (cursor.moveToPosition(position)) {

                int item_today = getItemIsToday(position, cursor);

                int prev_item_today = getItemIsToday(position - 1, cursor);

//                Event prev_item = (Event) super.getItem(position - 1);
//                Event item = (Event) super.getItem(position);

                if (item_today == prev_item_today) {
                    ret = TYPE_NORMAL;

                } else {
                    if (item_today == 1 && prev_item_today == 2) {
                        ret = TYPE_NORMAL;
                    } else {
                        ret = TYPE_HEADER;
                    }
                }
            }
        }

        return ret;
    }



}
