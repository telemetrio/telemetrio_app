package com.dbbest.telemetrioapp.unit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.test.AndroidTestCase;

import com.dbbest.telemetrioapp.activity.PhotoVideoActivity;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class PhotoVideoActivityTest extends AndroidTestCase {
    @Mock
    Context context;

    @Override
    protected void setUp() throws Exception {
//        System.setProperty("dexmaker.dexcache", "/data/data/com.dbbest.telemetrioapp/cache");
        System.setProperty("dexmaker.dexcache", getContext().getCacheDir().getPath());
        MockitoAnnotations.initMocks(this);
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, PhotoVideoActivity.class);
        intent.putExtra("ItemId", 1);
        return intent;
    }

    public void testIntent() throws Exception {
        Intent intent = getStartIntent(context);
        assertNotNull(intent);
        Bundle extras = intent.getExtras();
        assertNotNull(extras);
        assertEquals(1, extras.getInt("ItemId"));
    }


}