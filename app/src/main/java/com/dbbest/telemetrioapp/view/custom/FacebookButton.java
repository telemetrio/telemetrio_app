package com.dbbest.telemetrioapp.view.custom;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.util.Connectivity;
import com.dbbest.telemetrioapp.util.SocialUtils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import java.util.Arrays;

public class FacebookButton extends ImageView {
    private static final String TAG = "RequestTAG-FacebookButton";
    private Activity activity;
    private Callback callback;

    public FacebookButton(Context context) {
        super(context);
    }

    public FacebookButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FacebookButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){

    }

    public void initFacebookActions(final Activity activity, Callback callback){
        this.activity = activity;
        this.callback = callback;

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Connectivity.isConnected(getContext())){
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    return;
                }
                Session session = Session.getActiveSession();
                if (session != null && !session.isOpened() && !session.isClosed()) {
                    session.openForRead(new Session.OpenRequest(activity).
                            setPermissions(Arrays.asList("email")).setCallback(getStatusCallback()));
                }
                else {
                    Session.openActiveSession(activity, true, Arrays.asList("email"), getStatusCallback());
                }
            }
        });
    }

    private Session.StatusCallback getStatusCallback(){
        Session.StatusCallback statusCallback = new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (session.isOpened()) {
                    onOpenFacebookSession(session);
                }

            }
        };
        return statusCallback;
    }

    private void onOpenFacebookSession(final Session session){
        Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (user != null) {
                    if(user.getProperty("email") != null){
                        Log.d(TAG, user.getId());
                        Log.d(TAG, user.getProperty("email").toString());
                        Log.d(TAG, session.getAccessToken());
                        callback.onLoggedInFacebook(user.getId(), user.getUsername(), user.getProperty("email").toString(), user.getFirstName(), user.getLastName());
                    }
                    else {
                        Toast.makeText(getContext(), "Please add email to facebook account", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }).executeAsync();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        SocialUtils.Facebook.clearFacebookSession(activity);
    }


    public interface Callback{
        void onLoggedInFacebook(String userId, String username, String email, String firstName, String lastName);
    }

}
