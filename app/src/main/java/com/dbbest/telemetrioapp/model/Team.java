package com.dbbest.telemetrioapp.model;

/**
 * Created by Andrew Druk on 11/26/14.
 */
public class Team {

    public long idTeam;
    public long idRole;
    public String team_name;
    public String role_name;
    public String team_logo;

    @Override
    public String toString() {
        return "Team{" +
                "idTeam=" + idTeam +
                ", idRole=" + idRole +
                ", team_name='" + team_name + '\'' +
                ", role_name='" + role_name + '\'' +
                ", team_logo='" + team_logo + '\'' +
                '}';
    }
}
