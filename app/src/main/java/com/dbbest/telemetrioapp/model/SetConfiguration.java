package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;


public class SetConfiguration {

    @SerializedName("user_id")
    public long user_id;
    @SerializedName("setting")
    public long setting;

    @SerializedName("state")
    public long state;

    @Override
    public String toString() {
        return "SetBookmark{" +
                "user_id=" + user_id +
                ", setting=" + setting +
                ", state=" + state +
                '}';
    }
}
