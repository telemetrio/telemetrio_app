package com.dbbest.telemetrioapp.imageloader;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;

public class MD5FileNameGeneratorWithExtension extends Md5FileNameGenerator {

    @Override
    public String generate(String s) {
        return super.generate(s) + ".jpg";
    }
}
