package com.dbbest.telemetrioapp.util;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

public class ShareUtils {

    public static void share(Context context, File file) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/jpeg");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        context.startActivity(Intent.createChooser(sharingIntent, "Share image using"));
    }
}
