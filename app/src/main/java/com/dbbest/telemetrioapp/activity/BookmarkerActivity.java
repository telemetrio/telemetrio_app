package com.dbbest.telemetrioapp.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;
import com.dbbest.telemetrioapp.TelemetryApplication;
import com.dbbest.telemetrioapp.activity.base.FragmentActivityBackStack;
import com.dbbest.telemetrioapp.controller.SharedPreference;
import com.dbbest.telemetrioapp.model.Event;
import com.dbbest.telemetrioapp.model.SetBookmark;
import com.dbbest.telemetrioapp.provider.TelemetrioContract;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BookmarkerActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final String TAG = "BookmarkerActivity";
    private static final int REQUEST_CODE_RECORD_MEDIA = 11;


    private static final int URL_EVENT_LOADER = 0;
    private static final int SET_BOOKMARK_LOADER = 1;

//    private Context mContext;
    private FragmentActivityBackStack stack;
//    private Integer mPosition;
    private Button icon_goal;
    private Button icon_g_play;
    private Button icon_improve;
    private Button icon_start;
    private com.dbbest.telemetrioapp.view.TimelineView mTimeline;
    private long mStartTime;
    private long mEndTime;
    private Date mEventTimeEnd;
    private Date mEventTimeBegin;
    private Event mEvent;
    private boolean isStarted;

    private String mUserId;
    private String mEventId;
    private SharedPreference sharedPreference;
    private long mActivityStartTime;
    private ImageView icon_photo;
    private ImageView icon_camera;


    private boolean isTest = false;
    private  int mEventTimeMin = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bookmarker_activity);
        TextView textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText(getString(R.string.bookmark).toUpperCase());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        View backButton = findViewById(R.id.toolbar_left_button);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookmarkerActivity.this.onBackPressed();
            }
        });

        sharedPreference = new SharedPreference(getBaseContext());
        mUserId = sharedPreference.getUserID();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
//            mPosition = bundle.getInt("position");
//            mUserId = bundle.getString("user_id");
            mEventId = bundle.getString("event_id");
            if(null != mEventId && !mEventId.equals("")) {
                loadEvent(mEventId);
            } else {
                isTest = true;
            }
        } else {
            isTest = true;
        }


        icon_photo = (ImageView) findViewById(R.id.icon_photo);
        icon_camera = (ImageView) findViewById(R.id.icon_camera);


        icon_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File mSaveDirectory = getExternalFilesDir("Media");
                Intent intent = new Intent(BookmarkerActivity.this, RecordingActivity.class);
                intent.putExtra(RecordingActivity.KEY_SAVE_PATH, mSaveDirectory.getAbsolutePath());
                intent.putExtra(RecordingActivity.KEY_START_WITH_PHOTO, true);
                startActivityForResult(intent, REQUEST_CODE_RECORD_MEDIA);
            }
        });

        icon_camera .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File mSaveDirectory = getExternalFilesDir("Media");
                Intent intent = new Intent(BookmarkerActivity.this, RecordingActivity.class);
                intent.putExtra(RecordingActivity.KEY_SAVE_PATH, mSaveDirectory.getAbsolutePath());
                intent.putExtra(RecordingActivity.KEY_START_WITH_PHOTO, false);
                startActivityForResult(intent, REQUEST_CODE_RECORD_MEDIA);
            }
        });

        icon_goal = (Button) findViewById(R.id.icon_goal);
        icon_g_play = (Button) findViewById(R.id.icon_g_play);
        icon_improve = (Button) findViewById(R.id.icon_improve);
        icon_start = (Button) findViewById(R.id.icon_start);


        icon_g_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSlider(2);
            }
        });
        icon_improve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSlider(3);
            }
        });
        icon_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSlider(4);
            }
        });
        icon_goal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSlider(1);
            }
        });


        mTimeline = (com.dbbest.telemetrioapp.view.TimelineView) findViewById(R.id.time_line);

        if(isTest) {
            mTimeline.setTotalTime(120);
            mEventId = "10";
            loadEvent(mEventId);

        } else {

            mStartTime = mEvent.startTime.getTime() / 1000L;
            mEndTime = mEvent.endTime.getTime() / 1000L;

            long diffInSec = mEndTime - mStartTime;
            mEventTimeMin = (int) TimeUnit.SECONDS.toMinutes(diffInSec);

            mTimeline.setTotalTime(mEventTimeMin);
        }

//        getSupportLoaderManager().initLoader(0, null, this);

        mActivityStartTime = System.currentTimeMillis() / 1000L;


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadBookmarks(mEventId);

            }
        }, 1000);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE_RECORD_MEDIA){
            if (resultCode == Activity.RESULT_OK) {


                Boolean isVideo = data.getBooleanExtra(RecordingActivity.KEY_IS_VIDEO, false);
                String path = data.getStringExtra(RecordingActivity.KEY_RESULT_FILE_PATH);

                String photoId = TelemetryApplication.randomString(5);


                //TODO: fill record with correct data
                ContentValues values = new ContentValues();
                values.put(TelemetrioContract.Photos.PHOTO_ID, photoId);
                values.put(TelemetrioContract.Photos.PHOTO_LOCAL_ID, photoId);
                values.put(TelemetrioContract.Photos.PHOTO_IMAGE, isVideo ? 0 : 1);
                values.put(TelemetrioContract.Photos.PHOTO_THUMBNAIL, path);
                values.put(TelemetrioContract.Photos.PHOTO_FILE, path);
                values.put(TelemetrioContract.Photos.PHOTO_USER_ID, mUserId);
                values.put(TelemetrioContract.Photos.PHOTO_CLUB_ID, "13");
                values.put(TelemetrioContract.Photos.PHOTO_TEAM_ID, "13");
                values.put(TelemetrioContract.Photos.PHOTO_UPLOAD_STATUS, "0");
                getContentResolver().insert(TelemetrioContract.Photos.CONTENT_URI, values);
                Toast.makeText(this, ((isVideo) ? "Video " : "Photo ") + " was saved at " + path, Toast.LENGTH_SHORT).show();

                File mSaveDirectory = getExternalFilesDir("Media");
                RecordingActivity.start(BookmarkerActivity.this, mSaveDirectory.getAbsolutePath(), mEventId, REQUEST_CODE_RECORD_MEDIA);
            }
        }
    }
    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
    public void addSlider(final Integer type) {

        long currTime = (System.currentTimeMillis() / 1000L);

        if(4 == type) {
            isStarted = !isStarted;

            if(isStarted) {
                icon_start.setText(getText(R.string.icon_end));
            } else {
                icon_start.setText(getText(R.string.icon_start));
            }
        }

        String dateFormat = "dd, MMM yyyy HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Calendar mydate = Calendar.getInstance();


        mydate.setTimeInMillis(currTime*1000);
        Date mEventTime = mydate.getTime();
        Log.d("time_log", sdf.format(mEventTime));

        mydate.setTimeInMillis(mStartTime*1000);
        Date mEventTime2 = mydate.getTime();
        Log.d("time_log", sdf.format(mEventTime2));

        mydate.setTimeInMillis(mEndTime*1000);
        Date mEventTime3 = mydate.getTime();
        Log.d("time_log", sdf.format(mEventTime3));


        if((isTest) ||  (currTime > mStartTime && currTime < mEndTime)) {

            long currPointTime = currTime - mStartTime;
            if(isTest) {
                currPointTime = currTime - mActivityStartTime;
            }

            mTimeline.addPoint((int) currPointTime, type);


            if (null != mUserId && !mUserId.equals("") && null != mEventId && !mEventId.equals("")) {
                final double lat = sharedPreference.getLatitude();
                final double lon = sharedPreference.getLongitude();

                final long finalCurrPointTime = currPointTime;
                                TelemetryApplication.getRestService(sharedPreference.getAuthKey(), sharedPreference.getUserID(), RestAdapter.LogLevel.FULL).
                        setBookmark(mUserId, mEventId, type.toString(), lat, lon, new Callback<SetBookmark>() {
                            @Override
                            public void success(SetBookmark bookmark, Response response) {
    //                        if(isActive) {
                                Log.d("TAG", bookmark.toString());

                                bookmark.event_id = Long.valueOf(mEventId);
                                bookmark.bookmark_action = type;
                                bookmark.bookmark_time = (int) finalCurrPointTime;
                                bookmark.bookmark_location_lat = lat;
                                bookmark.bookmark_location_long = lon;

                                saveBookmark(bookmark);
//                              showToast("bookmark " + bookmark.bookmark_id);
    //                          mProgressBar.setVisibility(View.INVISIBLE);
//                              mSharedPreference.setUserID(String.valueOf(user.id));
//
//                              Intent startHomeActivity = new Intent(mContext, MainTabActivity.class);
//                              startActivity(startHomeActivity);
//                              getActivity().finish();
                            }
                            @Override
                            public void failure(RetrofitError error) {
//                          if(isActive) {
                                Log.d("TAG", error.toString());
                                showToast("Server ERROR");
//                              mProgressBar.setVisibility(View.INVISIBLE);
//                          }
                            }
                        });
                }
            } else {
            showToast(getString(R.string.during_game));
            }



//        long currTime = (System.currentTimeMillis() / 1000L) -mStartTime;
//        mTimeline.addPoint((int) currTime, type);

    }

    private void saveBookmark(SetBookmark bookmark){
        ContentValues values = new ContentValues();
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_ID, bookmark.bookmark_id);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID, bookmark.event_id);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_ACTION, bookmark.bookmark_action);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_TIME, bookmark.bookmark_time);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_LAT, bookmark.bookmark_location_lat);
        values.put(TelemetrioContract.Bookmarks.BOOKMARK_LONG, bookmark.bookmark_location_long);

        getContentResolver().insert(TelemetrioContract.Bookmarks.CONTENT_URI, values);
//        Toast.makeText(this, ((isVideo) ? "Video " : "Photo ") + " was saved at " + path, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
		super.onResume();

	}



	@Override
    public void onPause() {
		super.onPause();

	}

	
	private void showToast(String message) {
			Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
	}

    // These are the Bookmarks rows that we will retrieve.
    static final String[] BOOKMARKS_SUMMARY_PROJECTION = new String[] {


            TelemetrioContract.Bookmarks._ID,
            TelemetrioContract.Bookmarks.BOOKMARK_ID,
            TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID,
            TelemetrioContract.Bookmarks.BOOKMARK_ACTION,
            TelemetrioContract.Bookmarks.BOOKMARK_TIME,
            TelemetrioContract.Bookmarks.BOOKMARK_LAT,
            TelemetrioContract.Bookmarks.BOOKMARK_LONG,
            TelemetrioContract.Bookmarks.BOOKMARK_ACCURACY,

    };

    // These are the Events rows that we will retrieve.
    static final String[] EVENTS_SUMMARY_PROJECTION = new String[] {
            TelemetrioContract.Events._ID,
            TelemetrioContract.Events.EVENT_LEAGUE_NAME,
            TelemetrioContract.Events.EVENT_FIELD_NAME,
            TelemetrioContract.Events.EVENT_TIME_BEGIN,
            TelemetrioContract.Events.EVENT_TIME_END,
            TelemetrioContract.Events.EVENT_FIRST_TEAM_NAME,
            TelemetrioContract.Events.EVENT_SECOND_TEAM_NAME,
            TelemetrioContract.Events.EVENT_FIELD_ID,
            TelemetrioContract.Events.EVENT_ID_GAME,

            TelemetrioContract.Events.EVENT_TYPE,
            TelemetrioContract.Events.EVENT_TODAY,

    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.

        if (id == URL_EVENT_LOADER) {
            Uri baseUri = TelemetrioContract.Events.CONTENT_URI;

//            if (null != mPosition) {
//                String SELECTION = TelemetrioContract.Events._ID + "=?";
//                List<String> selectionArgs = new ArrayList<String>();
//                selectionArgs.add(mPosition.toString());
//
//
//                final String[] SELECTION_ARGS = new String[selectionArgs.size()];
//                selectionArgs.toArray(SELECTION_ARGS);
//                return new CursorLoader(this, baseUri,
//                        EVENTS_SUMMARY_PROJECTION, SELECTION, SELECTION_ARGS,
//                        TelemetrioContract.Events.DEFAULT_SORT);
//            } else {


            return new CursorLoader(this, baseUri,
                    EVENTS_SUMMARY_PROJECTION, null, null,
                    TelemetrioContract.Events.DEFAULT_SORT);
//            }
//        } else if (id == SET_BOOKMARK_LOADER) {

        }
        else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
/*
        if (cursor.moveToPosition(mPosition)) {

            String dateFormat = "dd, MMM yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

            String timeFormat = "HH:mm";
            SimpleDateFormat stf = new SimpleDateFormat(timeFormat);

            String eventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String eventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            mEventId = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_ID_GAME));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(eventTimeBegin));
            mEventTimeBegin = calEventTimeBegin.getTime();


            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(eventTimeEnd));
            mEventTimeEnd = calEventTimeEnd.getTime();

            mEvent = new Event();
            mEvent.endTime = mEventTimeEnd;
            mEvent.startTime = mEventTimeBegin;

            mStartTime = mEventTimeBegin.getTime() / 1000L;
            mEndTime = mEventTimeEnd.getTime() / 1000L;

            long diffInMs = mEventTimeEnd.getTime() - mEventTimeBegin.getTime();
            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);

//            showToast( stf.format(mEventTimeBegin ) + " - " + stf.format(mEventTimeEnd ) + " - " + diffInMs);

        }

        */
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void loadEvent(String event_id) {
        final Uri baseUri = TelemetrioContract.Events.CONTENT_URI;
        String WHERE =  TelemetrioContract.Events.EVENT_ID_GAME + "=?";
        String[] ARGS =  {event_id};
        Cursor cursor = getContentResolver().query(baseUri, EVENTS_SUMMARY_PROJECTION, WHERE, ARGS, TelemetrioContract.Events.DEFAULT_SORT);


        mEvent = new Event();

        while (cursor.moveToNext()) {

            String sEventTimeBegin = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_BEGIN));
            String sEventTimeEnd = cursor.getString(cursor.getColumnIndex(TelemetrioContract.Events.EVENT_TIME_END));

            Calendar calEventTimeBegin = Calendar.getInstance();
            calEventTimeBegin.setTimeInMillis(Long.valueOf(sEventTimeBegin));

            Calendar calEventTimeEnd = Calendar.getInstance();
            calEventTimeEnd.setTimeInMillis(Long.valueOf(sEventTimeEnd));

            Date eventTimeBegin = calEventTimeBegin.getTime();
            Date eventTimeEnd = calEventTimeEnd.getTime();

            int intEventId = Integer.valueOf(event_id);

            mEvent.idGame = intEventId;
            mEvent.startTime = eventTimeBegin;
            mEvent.endTime = eventTimeEnd;
        }
        cursor.close();
    }

    private void loadBookmarks(String event_id) {
        final Uri baseUri = TelemetrioContract.Bookmarks.CONTENT_URI;
        String WHERE =  TelemetrioContract.Bookmarks.BOOKMARK_EVENT_ID + "=?";
        String[] ARGS =  {event_id};
        Cursor cursor = getContentResolver().query(baseUri, BOOKMARKS_SUMMARY_PROJECTION, WHERE, ARGS, TelemetrioContract.Bookmarks.DEFAULT_SORT);


//        SetBookmark bookmark = new SetBookmark();

        while (cursor.moveToNext()) {

            long action = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_ACTION));
            long time = cursor.getLong(cursor.getColumnIndex(TelemetrioContract.Bookmarks.BOOKMARK_TIME));

            try {
                mTimeline.addPoint((int) time, (int) action);

            } catch (Exception e) {
                e.printStackTrace();
                showToast( e.getMessage());
            }


        }
        cursor.close();
    }

}
