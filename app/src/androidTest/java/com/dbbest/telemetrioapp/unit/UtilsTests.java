package com.dbbest.telemetrioapp.unit;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.dbbest.telemetrioapp.imageloader.MD5FileNameGeneratorWithExtension;

import org.mockito.Mockito;


public class UtilsTests extends AndroidTestCase {

    @SmallTest
    public void testFileNameHasJpegExtension() {
        MD5FileNameGeneratorWithExtension md5FileNameGeneratorWithExtension = new MD5FileNameGeneratorWithExtension();
        String fileName = md5FileNameGeneratorWithExtension.generate("test");
        assertTrue(fileName.endsWith(".jpg"));
    }


}
