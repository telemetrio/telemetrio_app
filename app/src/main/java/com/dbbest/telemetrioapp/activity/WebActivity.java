package com.dbbest.telemetrioapp.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.dbbest.telemetrioapp.R;

public class WebActivity extends FragmentActivity {

	private static final String TAG = "WebActivity";
    private WebView mWebview;
    private int mPage;
    private TextView mTitleTextView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mPage = bundle.getInt("page");
        }


        setContentView(R.layout.web_activity);
        mTitleTextView = (TextView) findViewById(R.id.toolbar_title);

        View backButton = findViewById(R.id.toolbar_left_button);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebActivity.this.onBackPressed();
            }
        });


        mWebview = (WebView) findViewById(R.id.webview);
        mWebview.getSettings().setJavaScriptEnabled(true);

        loadAssets(mPage);
//        addZoomSupport();


    }

    private void loadAssets(int page) {
        switch (page) {
            case SettingsActivity.PAGE_FAQ:
                mTitleTextView.setText(getString(R.string.faq).toUpperCase());
                mWebview.loadUrl("file:///android_asset/faq.html");
            break;
            case SettingsActivity.PAGE_TOS:
                mTitleTextView.setText(getString(R.string.tos).toUpperCase());
                mWebview.loadUrl("file:///android_asset/tos.html");
                break;

            default:
            break;
        }

    }


        @Override
    public void onResume() {
		super.onResume();

	}



	@Override
    public void onPause() {
		super.onPause();

	}

	private void showToast(String message) {
			Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
	}

    private void addZoomSupport() {
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        mWebview.getSettings().setDefaultZoom(ZoomDensity.FAR);
        mWebview.setInitialScale(100);
    }


}
