/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dbbest.telemetrioapp.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Provider that stores {@link com.dbbest.telemetrioapp.provider.TelemetrioContract} data. Data is usually inserted
 * by {@link com.dbbest.telemetrioapp.service.UpdateService}, and queried by various
 * {@link android.app.Activity} instances.
 */
public class TelemetrioProvider extends ContentProvider {
    private static final String TAG = "Provider";

    private TelemetrioDatabase mOpenHelper;

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int EVENTS = 100;
    private static final int EVENTS_ID = 101;
    private static final int PHOTOS = 102;
    private static final int PHOTOS_ID = 103;
    private static final int BOOKMARKS = 104;
    private static final int BOOKMARKS_ID = 105;


    /**
     * Build and return a {@link android.content.UriMatcher} that catches all {@link android.net.Uri}
     * variations supported by this {@link android.content.ContentProvider}.
     */
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = TelemetrioContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, "events", EVENTS);
        matcher.addURI(authority, "events/*", EVENTS_ID);
        matcher.addURI(authority, "photos", PHOTOS);
        matcher.addURI(authority, "photos/*", PHOTOS_ID);
        matcher.addURI(authority, "bookmarks", BOOKMARKS);
        matcher.addURI(authority, "bookmarks/*", BOOKMARKS_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new TelemetrioDatabase(getContext());
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case EVENTS:
                return TelemetrioContract.Events.CONTENT_TYPE;
            case EVENTS_ID:
                return TelemetrioContract.Events.CONTENT_ITEM_TYPE;
            case PHOTOS:
                return TelemetrioContract.Photos.CONTENT_TYPE;
            case PHOTOS_ID:
                return TelemetrioContract.Photos.CONTENT_ITEM_TYPE;
            case BOOKMARKS:
                return TelemetrioContract.Bookmarks.CONTENT_TYPE;
            case BOOKMARKS_ID:
                return TelemetrioContract.Bookmarks.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {

        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        final int match = sUriMatcher.match(uri);

        switch (match) {
            default: {
                // Most cases are handled with simple SelectionBuilder
                final SelectionBuilder builder = buildSimpleSelection(uri);
                Cursor cursor = builder
                        .where(selection, selectionArgs)
                        .query(db, false, projection, sortOrder, null);
                Context context = getContext();
                if (null != context) {
                    cursor.setNotificationUri(context.getContentResolver(), uri);
                }
                return cursor;
            }
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case EVENTS: {
                long id = db.insertOrThrow(TelemetrioDatabase.Tables.EVENTS, null, values);
                notifyChange(uri);
                return TelemetrioContract.Events.buildEventUri(id + "");

            }

            case PHOTOS: {
                long id = db.insertOrThrow(TelemetrioDatabase.Tables.PHOTOS, null, values);
                notifyChange(uri);
                return TelemetrioContract.Photos.buildPhotoUri(id + "");
            }
            case BOOKMARKS: {
                long id = db.insertOrThrow(TelemetrioDatabase.Tables.BOOKMARKS, null, values);
                notifyChange(uri);
                return TelemetrioContract.Bookmarks.buildBookmarkUri(id + "");
            }
            default: {
                throw new UnsupportedOperationException("Unknown insert uri: " + uri);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).update(db, values);
        notifyChange(uri);
        return retVal;
    }

    /** {@inheritDoc} */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        final int match = sUriMatcher.match(uri);
        int retVal = builder.where(selection, selectionArgs).delete(db);
        notifyChange(uri);
        return retVal;
    }

    private void notifyChange(Uri uri) {
        // We only notify changes if the caller is not the sync adapter.
        // The sync adapter has the responsibility of notifying changes (it can do so
        // more intelligently than we can -- for example, doing it only once at the end
        // of the sync instead of issuing thousands of notifications for each record).
        Context context = getContext();
        context.getContentResolver().notifyChange(uri, null);

    }

    /**
     * Apply the given set of {@link android.content.ContentProviderOperation}, executing inside
     * a {@link android.database.sqlite.SQLiteDatabase} transaction. All changes will be rolled back if
     * any single one fails.
     */
    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Build a simple {@link SelectionBuilder} to match the requested
     * {@link android.net.Uri}. This is usually enough to support {@link #insert},
     * {@link #update}, and {@link #delete} operations.
     */
    private SelectionBuilder buildSimpleSelection(Uri uri) {
        final SelectionBuilder builder = new SelectionBuilder();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case EVENTS: {
                return builder.table(TelemetrioDatabase.Tables.EVENTS);
            }
            case EVENTS_ID: {
                final String blockId = TelemetrioContract.Events.geEventId(uri);
                return builder.table(TelemetrioDatabase.Tables.EVENTS)
                        .where(TelemetrioContract.Events._ID + "=?", blockId);
            }
            case PHOTOS: {
                return builder.table(TelemetrioDatabase.Tables.PHOTOS);
            }
            case PHOTOS_ID: {
                final String blockId = TelemetrioContract.Events.geEventId(uri);
                return builder.table(TelemetrioDatabase.Tables.PHOTOS)
                        .where(TelemetrioContract.Photos._ID + "=?", blockId);
            }
            case BOOKMARKS: {
                return builder.table(TelemetrioDatabase.Tables.BOOKMARKS);
            }
            case BOOKMARKS_ID: {
                final String blockId = TelemetrioContract.Bookmarks.geBookmarkId(uri);
                return builder.table(TelemetrioDatabase.Tables.BOOKMARKS)
                        .where(TelemetrioContract.Bookmarks._ID + "=?", blockId);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri for " + match + ": " + uri);
            }
        }
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }
}
