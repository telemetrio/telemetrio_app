package com.dbbest.telemetrioapp.service;

public enum RequestState {

	PENDING,
	COMPLETED,
	ERROR
}
