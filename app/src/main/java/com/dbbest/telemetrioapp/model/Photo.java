package com.dbbest.telemetrioapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Andrew Druk on 11/25/14.
 */
public class Photo {

    public static final int SERVER_FILE_STATUS = 1;
    public static final int NOT_UPLOADED_STATUS = 0;
    public static final int UPLOADED_STATUS = 1;

    public long status;

    @SerializedName("id_file")
    public long fileId;            //File identifier
    @SerializedName("image")
    public int image;
    @SerializedName("file")
    public String fileUrl;         //Image address
    @SerializedName("id_user")
    public long userId;            //User owner identifier
    @SerializedName("id_club")
    public long clubId;            //Club identifier
    @SerializedName("id_team")
    public long teamId;            //Team identifier
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("description")
    public String description;

    public boolean isVideo() {
        return image == 0;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "fileId=" + fileId +
                ", image=" + image +
                ", fileUrl='" + fileUrl + '\'' +
                ", userId=" + userId +
                ", clubId=" + clubId +
                ", teamId=" + teamId +
                ", description=" + description +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
